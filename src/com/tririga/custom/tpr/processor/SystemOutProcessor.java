package com.tririga.custom.tpr.processor;

import java.util.Collection;

import com.tririga.custom.tpr.data.TprData;

public class SystemOutProcessor extends Observer {

	public SystemOutProcessor(boolean register) {
		super(register);
	}

	@Override
	public void process(TprData tprData) {
		System.out.println("SystemOutProcessor:" + tprData);

	}

	@Override
	public Collection<?> getDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

}
