package com.tririga.custom.tpr.processor.timesum;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;

import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.tririga.custom.tpr.processor.summary.SummaryData;
import com.tririga.custom.tpr.reports.DataSourceI;
import com.tririga.custom.tpr.reports.system.SummaryStore;

// TODO: Auto-generated Javadoc
/**
 * The Class FileStore.
 */
public class TimeSummaryFileStore implements SummaryStore {

	/** The output file name. */
	private String outputFileName = "temp.txt";
	private static final String outputDir = "output";

	/** The sum processors. */
	private final CellProcessor[] sum_processors = new CellProcessor[] {
			new NotNull(), // DateKey header
			new NotNull(), // Type header
			new NotNull(), // Key number
			new ParseInt(), // MinVal
			new ParseInt(), // MaxVal
			new ParseInt(), // total Count
			new ParseInt() }; // total Duration

	private final String[] header = new String[] { "date", "type", "key",
			"minVal", "maxVal", "totalDuration", "totalCount" };

	/**
	 * Store.
	 * 
	 * @throws Exception
	 * @throws Exception
	 */
	public void store(Collection<SummaryData> col) throws Exception {
		System.out.println("FileStore...");
		ICsvBeanWriter beanWriter = null;

		try {
			// create the files and direct
			File dir = new File(outputDir);
			dir.mkdir();

			beanWriter = new CsvBeanWriter(new FileWriter(outputDir + "/"
					+ outputFileName), CsvPreference.STANDARD_PREFERENCE);

			beanWriter.writeHeader(header);

			// write the beans
			for (final DataSourceI customer : col) {
				beanWriter.write(customer, header, sum_processors);
			}
		} finally {
			if (beanWriter != null) {
				beanWriter.close();
			}
		}
	}

	/**
	 * Gets the output file name.
	 * 
	 * @return the output file name
	 */
	public String getOutputFileName() {
		return outputFileName;

	}

	/**
	 * Sets the output file name.
	 * 
	 * @param outputFileName
	 *            the new output file name
	 */
	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}

}
