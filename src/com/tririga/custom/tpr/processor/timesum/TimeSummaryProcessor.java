package com.tririga.custom.tpr.processor.timesum;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.tririga.custom.tpr.data.TprData;
import com.tririga.custom.tpr.data.TypeEnum;
import com.tririga.custom.tpr.processor.Observer;
import com.tririga.custom.tpr.processor.summary.SummaryData;
import com.tririga.custom.tpr.processor.summary.SummaryProcessor;
import com.tririga.custom.tpr.processor.util.DataUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class SummaryProcessor.
 */
public class TimeSummaryProcessor extends Observer {

	/** The summary. */
	private HashMap<Date, SummaryProcessor> summary = new HashMap<Date, SummaryProcessor>();
	private int threashold1;
	private int threashold2;
	
	private TimeRangeEnum timeRange = null;
	private List<SummaryData> elementsList = null;
	private List<TypeEnum> excludeElementTypes;	
	private List<String> excludeElements;
	/**
	 * Instantiates a new summary processor.
	 */
	public TimeSummaryProcessor(TimeRangeEnum timeRange, List<SummaryData> elementsList, int threashold1, int threashold2, List<TypeEnum> excludeElementTypes,
								List<String> excludeElements) {
		super(true);
		this.timeRange = timeRange;
		this.elementsList = elementsList;
		this.threashold1 = threashold1;
		this.threashold2 = threashold2;
		this.excludeElementTypes = excludeElementTypes;
		this.excludeElements = excludeElements;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tririga.custom.tpr.processor.Observer#process(com.tririga.custom.
	 * tpr.data.TprData)
	 */
	@Override
	public void process(TprData tprData) throws Exception {
		if(!this.excludeElementTypes.contains(TypeEnum.getTypeEnum(tprData.getType()))) {
			DataUtil dUtil = new DataUtil();
			
			Date dateKey = dUtil.getDateKey(tprData.getStartTime(),timeRange);
			SummaryProcessor activeSummary = summary.get(dateKey);
	
			if (activeSummary == null) {
				activeSummary = getNewSummaryProcessor(dateKey, tprData);
			}
			
			activeSummary.process(tprData);
			summary.put(dateKey, activeSummary);
		}
	}


	/**
	 * Gets the new summary processor.
	 * 
	 * @param dateKey
	 *            the date key
	 * @return the new summary processor
	 * @throws Exception 
	 */
	private SummaryProcessor getNewSummaryProcessor(Date dateKey, TprData tprData) throws Exception {
		// Set up parms for Processors and Stores
		SummaryProcessor sumProc = null;

		sumProc = new SummaryProcessor(false, dateKey,threashold1,threashold2, this.excludeElementTypes, excludeElements, timeRange );
		//sumProc = summary.get(dateKey);
		
		return sumProc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tririga.custom.tpr.processor.Observer#store()
	 */

	@Override
	public Collection<?> getDataSource() {
		ArrayList<SummaryData> rtnAl = new ArrayList<SummaryData>();
		
		if(this.elementsList == null) {
			Collection<SummaryProcessor> col = summary.values();
			for (SummaryProcessor proc : col) {
				rtnAl.addAll((Collection<? extends SummaryData>) proc.getDataSource());
			}
			
		} else {
			Collection<SummaryProcessor> col = summary.values();
			for (SummaryProcessor proc : col) {
				// should do a better search
				for(SummaryData sumData: (Collection<? extends SummaryData>)proc.getDataSource()) {
					for(SummaryData data:this.elementsList) {
						if(sumData.getKey().equals(data.getKey())) {
							System.out.println("sumData:"+sumData);
							rtnAl.add(sumData);
						}
					}
				}
			}
			
		}

		return rtnAl;
	}

}
