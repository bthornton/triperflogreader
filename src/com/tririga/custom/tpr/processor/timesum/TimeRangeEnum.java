package com.tririga.custom.tpr.processor.timesum;

public enum TimeRangeEnum {
	WEEK, DAY, HOUR, DAY_NO_HOUR
}
