package com.tririga.custom.tpr.processor.totalsum;

import java.util.Calendar;
import java.util.TimeZone;

import com.tririga.custom.tpr.data.TprData;
import com.tririga.custom.tpr.reports.DataSourceI;

// TODO: Auto-generated Javadoc
/**
 * The Class TotalSummaryData.
 */
public class TotalSummaryData implements DataSourceI {

	private TprData firstRecord;
	private TprData lastRecord;

	/** The total reports. */
	private int reportsTotal;

	/** The reports dur total. */
	private int reportsDurTotal;

	/** The longest duration report. */
	private int reportLongestDuration;

	/** The shortest duration report. */
	private int reportShortestDuration;

	/** The total workflow async. */
	private int workflowAsyncTotal;

	/** The workflow async dur total. */
	private int workflowAsyncDurTotal;

	/** The longest duration workflow async. */
	private int workflowAsyncLongestDuration;

	/** The shortest duration workflow async. */
	private int workflowAsyncShortestDuration;

	/** The total workflow sync. */
	private int workflowSyncTotal;

	/** The workflow sync dur total. */
	private int workflowSyncDurTotal;

	/** The longest duration workflow sync. */
	private int workflowSyncLongestDuration;

	/** The shortest duration workflow sync. */
	private int workflowSyncShortestDuration;

	/** The total ext formula queue. */
	private int extFormulaQueueTotal;

	/** The ext formula queue dur total. */
	private int extFormulaQueueDurTotal;

	/** The longest duration ext formula queue. */
	private int extFormulaQueueLongestDuration;

	/** The shortest duration ext formula queue. */
	private int extFormulaQueueShortestDuration;

	/** The total so transition. */
	private int sOTransitionTotal;

	/** The s o transition dur total. */
	private int sOTransitionDurTotal;

	/** The longest duration so transition. */
	private int sOTransitionLongestDuration;

	/** The shortest duration so transition. */
	private int sOTransitionShortestDuration;

	/** The total ext formula calc. */
	private int extFormulaCalcTotal;

	/** The ext formula calc dur total. */
	private int extFormulaCalcDurTotal;

	/** The longest duration ext formula calc. */
	private int extFormulaCalcLongestDuration;

	/** The shortest duration ext formula calc. */
	private int extFormulaCalcShortestDuration;

	/** The total. */
	private int total;

	/** The total dur. */
	private int totalDur;

	/** The total unknown types. */
	private int totalUnknownTypes;

	/** The total unknown dur. */
	private int totalUnknownDur;

	public TprData getFirstRecord() {
		return firstRecord;
	}

	public void setFirstRecord(TprData firstRecord) {
		this.firstRecord = firstRecord;
	}

	public TprData getLastRecord() {
		return lastRecord;
	}

	public void setLastRecord(TprData lastRecord) {
		this.lastRecord = lastRecord;
	}

	public String getStartDate() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(getFirstRecord().getStartTime());

		return "" + cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH)+1)
				+ "/" + cal.get(Calendar.DAY_OF_MONTH) + " "
				+ cal.get(Calendar.HOUR_OF_DAY) + ":"
				+ cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
	}

	public String getEndDate() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(this.getLastRecord().getEndTime());

		return "" + cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH)+1)
				+ "/" + cal.get(Calendar.DAY_OF_MONTH) + " "
				+ cal.get(Calendar.HOUR_OF_DAY) + ":"
				+ cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
	}
	
	public long getReportDurationDays () {
		long rtnVal = this.getLastRecord().getEndTime() - this.getFirstRecord().getStartTime();
		
		//sec
		rtnVal = (rtnVal/1000);
		
		//min
		rtnVal = (rtnVal/60);

		//hours
		rtnVal = (rtnVal/60);

		//days
		rtnVal = (rtnVal/24);
		
		// round up 
		// should be a double and use the round feature in java to return a long
		rtnVal = rtnVal+1;
		
		return rtnVal;
		
	}

	/**
	 * Sets the total reports.
	 * 
	 * @param duration
	 *            the duration
	 */
	public void incReportsTotal(int duration) {
		this.reportsTotal = reportsTotal + 1;
		this.reportsDurTotal = this.reportsDurTotal + duration;
	}

	/**
	 * Sets the workflow async total.
	 * 
	 * @param duration
	 *            the duration
	 */
	public void incWorkflowAsyncTotal(int duration) {
		workflowAsyncTotal = workflowAsyncTotal + 1;
		this.workflowAsyncDurTotal = workflowAsyncDurTotal + duration;
	}

	/**
	 * Sets the workflow sync total.
	 * 
	 * @param duration
	 *            the duration
	 */
	public void incWorkflowSyncTotal(int duration) {
		workflowSyncTotal = workflowSyncTotal + 1;
		this.workflowSyncDurTotal = workflowSyncDurTotal + duration;
	}

	/**
	 * Sets the ext formula queue total.
	 * 
	 * @param duration
	 *            the duration
	 */
	public void incExtFormulaQueueTotal(int duration) {
		extFormulaQueueTotal = extFormulaQueueTotal + 1;
		this.extFormulaQueueDurTotal = extFormulaQueueDurTotal + duration;
	}

	/**
	 * Sets the SO transition total.
	 * 
	 * @param duration
	 *            the duration
	 */
	public void incSOTransitionTotal(int duration) {
		sOTransitionTotal = sOTransitionTotal + 1;
		this.sOTransitionDurTotal = sOTransitionDurTotal + duration;
	}

	/**
	 * Sets the ext formula calc total.
	 * 
	 * @param duration
	 *            the duration
	 */
	public void incExtFormulaCalcTotal(int duration) {
		extFormulaCalcTotal = extFormulaCalcTotal + 1;
		this.extFormulaCalcDurTotal = extFormulaCalcDurTotal + duration;
	}

	/**
	 * Sets the total.
	 * 
	 * @param duration
	 *            the duration
	 */
	public void incTotal(int duration) {
		this.total = total + 1;
		this.totalDur = totalDur + duration;
	}

	/**
	 * Sets the total unknown types.
	 * 
	 * @param duration
	 *            the duration
	 */
	public void incTotalUnknownTypes(int duration) {
		this.totalUnknownTypes = totalUnknownTypes + 1;
		totalUnknownDur = totalUnknownDur + duration;
	}

	/**
	 * Gets the report longest duration.
	 * 
	 * @return the report longest duration
	 */
	public int getReportLongestDuration() {
		return reportLongestDuration;
	}

	/**
	 * Sets the report longest duration.
	 * 
	 * @param reportLongestDuration
	 *            the new report longest duration
	 */
	public void setReportLongestDuration(int reportLongestDuration) {
		this.reportLongestDuration = reportLongestDuration;
	}

	/**
	 * Gets the report shortest duration.
	 * 
	 * @return the report shortest duration
	 */
	public int getReportShortestDuration() {
		return reportShortestDuration;
	}

	/**
	 * Sets the report shortest duration.
	 * 
	 * @param reportShortestDuration
	 *            the new report shortest duration
	 */
	public void setReportShortestDuration(int reportShortestDuration) {
		this.reportShortestDuration = reportShortestDuration;
	}

	/**
	 * Gets the workflow async longest duration.
	 * 
	 * @return the workflow async longest duration
	 */
	public int getWorkflowAsyncLongestDuration() {
		return workflowAsyncLongestDuration;
	}

	/**
	 * Sets the workflow async longest duration.
	 * 
	 * @param workflowAsyncLongestDuration
	 *            the new workflow async longest duration
	 */
	public void setWorkflowAsyncLongestDuration(int workflowAsyncLongestDuration) {
		this.workflowAsyncLongestDuration = workflowAsyncLongestDuration;
	}

	/**
	 * Gets the workflow async shortest duration.
	 * 
	 * @return the workflow async shortest duration
	 */
	public int getWorkflowAsyncShortestDuration() {
		return workflowAsyncShortestDuration;
	}

	/**
	 * Sets the workflow async shortest duration.
	 * 
	 * @param workflowAsyncShortestDuration
	 *            the new workflow async shortest duration
	 */
	public void setWorkflowAsyncShortestDuration(
			int workflowAsyncShortestDuration) {
		this.workflowAsyncShortestDuration = workflowAsyncShortestDuration;
	}

	/**
	 * Gets the workflow sync longest duration.
	 * 
	 * @return the workflow sync longest duration
	 */
	public int getWorkflowSyncLongestDuration() {
		return workflowSyncLongestDuration;
	}

	/**
	 * Sets the workflow sync longest duration.
	 * 
	 * @param workflowSyncLongestDuration
	 *            the new workflow sync longest duration
	 */
	public void setWorkflowSyncLongestDuration(int workflowSyncLongestDuration) {
		this.workflowSyncLongestDuration = workflowSyncLongestDuration;
	}

	/**
	 * Gets the workflow sync shortest duration.
	 * 
	 * @return the workflow sync shortest duration
	 */
	public int getWorkflowSyncShortestDuration() {
		return workflowSyncShortestDuration;
	}

	/**
	 * Sets the workflow sync shortest duration.
	 * 
	 * @param workflowSyncShortestDuration
	 *            the new workflow sync shortest duration
	 */
	public void setWorkflowSyncShortestDuration(int workflowSyncShortestDuration) {
		this.workflowSyncShortestDuration = workflowSyncShortestDuration;
	}

	/**
	 * Gets the ext formula queue dur total.
	 * 
	 * @return the ext formula queue dur total
	 */
	public int getExtFormulaQueueDurTotal() {
		return extFormulaQueueDurTotal;
	}

	/**
	 * Sets the ext formula queue dur total.
	 * 
	 * @param extFormulaQueueDurTotal
	 *            the new ext formula queue dur total
	 */
	public void setExtFormulaQueueDurTotal(int extFormulaQueueDurTotal) {
		this.extFormulaQueueDurTotal = extFormulaQueueDurTotal;
	}

	/**
	 * Gets the ext formula queue longest duration.
	 * 
	 * @return the ext formula queue longest duration
	 */
	public int getExtFormulaQueueLongestDuration() {
		return extFormulaQueueLongestDuration;
	}

	/**
	 * Sets the ext formula queue longest duration.
	 * 
	 * @param extFormulaQueueLongestDuration
	 *            the new ext formula queue longest duration
	 */
	public void setExtFormulaQueueLongestDuration(
			int extFormulaQueueLongestDuration) {
		this.extFormulaQueueLongestDuration = extFormulaQueueLongestDuration;
	}

	/**
	 * Gets the ext formula queue shortest duration.
	 * 
	 * @return the ext formula queue shortest duration
	 */
	public int getExtFormulaQueueShortestDuration() {
		return extFormulaQueueShortestDuration;
	}

	/**
	 * Sets the ext formula queue shortest duration.
	 * 
	 * @param extFormulaQueueShortestDuration
	 *            the new ext formula queue shortest duration
	 */
	public void setExtFormulaQueueShortestDuration(
			int extFormulaQueueShortestDuration) {
		this.extFormulaQueueShortestDuration = extFormulaQueueShortestDuration;
	}

	/**
	 * Gets the s o transition longest duration.
	 * 
	 * @return the s o transition longest duration
	 */
	public int getsOTransitionLongestDuration() {
		return sOTransitionLongestDuration;
	}

	/**
	 * Sets the s o transition longest duration.
	 * 
	 * @param sOTransitionLongestDuration
	 *            the new s o transition longest duration
	 */
	public void setsOTransitionLongestDuration(int sOTransitionLongestDuration) {
		this.sOTransitionLongestDuration = sOTransitionLongestDuration;
	}

	/**
	 * Gets the s o transition shortest duration.
	 * 
	 * @return the s o transition shortest duration
	 */
	public int getsOTransitionShortestDuration() {
		return sOTransitionShortestDuration;
	}

	/**
	 * Sets the s o transition shortest duration.
	 * 
	 * @param sOTransitionShortestDuration
	 *            the new s o transition shortest duration
	 */
	public void setsOTransitionShortestDuration(int sOTransitionShortestDuration) {
		this.sOTransitionShortestDuration = sOTransitionShortestDuration;
	}

	/**
	 * Gets the ext formula calc longest duration.
	 * 
	 * @return the ext formula calc longest duration
	 */
	public int getExtFormulaCalcLongestDuration() {
		return extFormulaCalcLongestDuration;
	}

	/**
	 * Sets the ext formula calc longest duration.
	 * 
	 * @param extFormulaCalcLongestDuration
	 *            the new ext formula calc longest duration
	 */
	public void setExtFormulaCalcLongestDuration(
			int extFormulaCalcLongestDuration) {
		this.extFormulaCalcLongestDuration = extFormulaCalcLongestDuration;
	}

	/**
	 * Gets the ext formula calc shortest duration.
	 * 
	 * @return the ext formula calc shortest duration
	 */
	public int getExtFormulaCalcShortestDuration() {
		return extFormulaCalcShortestDuration;
	}

	/**
	 * Sets the ext formula calc shortest duration.
	 * 
	 * @param extFormulaCalcShortestDuration
	 *            the new ext formula calc shortest duration
	 */
	public void setExtFormulaCalcShortestDuration(
			int extFormulaCalcShortestDuration) {
		this.extFormulaCalcShortestDuration = extFormulaCalcShortestDuration;
	}

	/**
	 * Gets the reports total.
	 * 
	 * @return the reports total
	 */
	public int getReportsTotal() {
		return reportsTotal;
	}

	/**
	 * Gets the reports dur total.
	 * 
	 * @return the reports dur total
	 */
	public int getReportsDurTotal() {
		return reportsDurTotal;
	}

	/**
	 * Gets the workflow async total.
	 * 
	 * @return the workflow async total
	 */
	public int getWorkflowAsyncTotal() {
		return workflowAsyncTotal;
	}

	/**
	 * Gets the workflow async dur total.
	 * 
	 * @return the workflow async dur total
	 */
	public int getWorkflowAsyncDurTotal() {
		return workflowAsyncDurTotal;
	}

	/**
	 * Gets the workflow sync total.
	 * 
	 * @return the workflow sync total
	 */
	public int getWorkflowSyncTotal() {
		return workflowSyncTotal;
	}

	/**
	 * Gets the workflow sync dur total.
	 * 
	 * @return the workflow sync dur total
	 */
	public int getWorkflowSyncDurTotal() {
		return workflowSyncDurTotal;
	}

	/**
	 * Gets the ext formula queue total.
	 * 
	 * @return the ext formula queue total
	 */
	public int getExtFormulaQueueTotal() {
		return extFormulaQueueTotal;
	}

	/**
	 * Gets the s o transition total.
	 * 
	 * @return the s o transition total
	 */
	public int getsOTransitionTotal() {
		return sOTransitionTotal;
	}

	/**
	 * Gets the s o transition dur total.
	 * 
	 * @return the s o transition dur total
	 */
	public int getsOTransitionDurTotal() {
		return sOTransitionDurTotal;
	}

	/**
	 * Gets the ext formula calc total.
	 * 
	 * @return the ext formula calc total
	 */
	public int getExtFormulaCalcTotal() {
		return extFormulaCalcTotal;
	}

	/**
	 * Gets the ext formula calc dur total.
	 * 
	 * @return the ext formula calc dur total
	 */
	public int getExtFormulaCalcDurTotal() {
		return extFormulaCalcDurTotal;
	}

	/**
	 * Gets the total.
	 * 
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * Gets the total dur.
	 * 
	 * @return the total dur
	 */
	public int getTotalDur() {
		return totalDur;
	}

	/**
	 * Gets the total unknown types.
	 * 
	 * @return the total unknown types
	 */
	public int getTotalUnknownTypes() {
		return totalUnknownTypes;
	}

	/**
	 * Gets the total unknown dur.
	 * 
	 * @return the total unknown dur
	 */
	public int getTotalUnknownDur() {
		return totalUnknownDur;
	}

	@Override
	public String toString() {
		return "TotalSummaryData [reportsTotal=" + reportsTotal
				+ ", reportsDurTotal=" + reportsDurTotal
				+ ", reportLongestDuration=" + reportLongestDuration
				+ ", reportShortestDuration=" + reportShortestDuration
				+ ", workflowAsyncTotal=" + workflowAsyncTotal
				+ ", workflowAsyncDurTotal=" + workflowAsyncDurTotal
				+ ", workflowAsyncLongestDuration="
				+ workflowAsyncLongestDuration
				+ ", workflowAsyncShortestDuration="
				+ workflowAsyncShortestDuration + ", workflowSyncTotal="
				+ workflowSyncTotal + ", workflowSyncDurTotal="
				+ workflowSyncDurTotal + ", workflowSyncLongestDuration="
				+ workflowSyncLongestDuration
				+ ", workflowSyncShortestDuration="
				+ workflowSyncShortestDuration + ", extFormulaQueueTotal="
				+ extFormulaQueueTotal + ", extFormulaQueueDurTotal="
				+ extFormulaQueueDurTotal + ", extFormulaQueueLongestDuration="
				+ extFormulaQueueLongestDuration
				+ ", extFormulaQueueShortestDuration="
				+ extFormulaQueueShortestDuration + ", sOTransitionTotal="
				+ sOTransitionTotal + ", sOTransitionDurTotal="
				+ sOTransitionDurTotal + ", sOTransitionLongestDuration="
				+ sOTransitionLongestDuration
				+ ", sOTransitionShortestDuration="
				+ sOTransitionShortestDuration + ", extFormulaCalcTotal="
				+ extFormulaCalcTotal + ", extFormulaCalcDurTotal="
				+ extFormulaCalcDurTotal + ", extFormulaCalcLongestDuration="
				+ extFormulaCalcLongestDuration
				+ ", extFormulaCalcShortestDuration="
				+ extFormulaCalcShortestDuration + ", total=" + total
				+ ", totalDur=" + totalDur + ", totalUnknownTypes="
				+ totalUnknownTypes + ", totalUnknownDur=" + totalUnknownDur
				+ "]";
	}

}
