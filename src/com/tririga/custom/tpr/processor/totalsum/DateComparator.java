package com.tririga.custom.tpr.processor.totalsum;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class DateComparator implements Comparator<TotalSummaryData> {

	@Override
	public int compare(TotalSummaryData arg0, TotalSummaryData arg1) {
		  if (arg0 == arg1) {
		        return 0;
		    }
		    if (arg0 == null) {
		        return -1;
		    }
		    if (arg1 == null) {
		        return 1;
		    }
		 
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    String arg0Comp = sdf.format(new Date(arg0.getStartDate()));
		    String arg1Comp = sdf.format(new Date(arg1.getStartDate()));
		    return arg0Comp.compareTo(arg1Comp);


	}

}
