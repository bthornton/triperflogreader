package com.tririga.custom.tpr.processor.totalsum;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.tririga.custom.tpr.data.TprData;
import com.tririga.custom.tpr.data.TypeEnum;
import com.tririga.custom.tpr.processor.Observer;

public class TotalSummaryProcessor extends Observer {
	
	/** The instance. */
	private TotalSummaryData totalSumData = new TotalSummaryData();
	private List<TypeEnum> excludeElementTypes;
	
	//change to use the Enum class
	public static final String WORKFLOW_SYNC = "Workflow - Sync";
	public static final String EF_CALC = "Extended Formula - Calculation";
	public static final String SO_TRANS = "Smart Object Transition";
	public static final String REPORT = "Report";
	public static final String WORKFLOW_ASYNC = "Workflow - Async";

	public TotalSummaryProcessor(boolean register, List<TypeEnum> excludeElementTypes) {
		super(register);
		this.excludeElementTypes = excludeElementTypes;
	}

	@Override
	public void process(TprData tprData) throws Exception { 
		if(!this.excludeElementTypes.contains(TypeEnum.getTypeEnum(tprData.getType()))) {
			
			// set first and last record (for start and enddate
			if (totalSumData.getTotal() == 0) {
				totalSumData.setFirstRecord(tprData);
			}
			// Always set the last (corner case for file with 1 record
			totalSumData.setLastRecord(tprData);
	
			// count
			totalSumData.incTotal(tprData.getDuration());
	
			if (TotalSummaryProcessor.EF_CALC.equalsIgnoreCase(tprData.getType())) {
				totalSumData.incExtFormulaCalcTotal(tprData.getDuration());
	
				// set Longest and Shortest
				if (totalSumData.getExtFormulaCalcLongestDuration() < tprData
						.getDuration()) {
					totalSumData.setExtFormulaCalcLongestDuration(tprData
							.getDuration());
				} else if (totalSumData.getExtFormulaCalcShortestDuration() > tprData
						.getDuration()) {
					totalSumData.setExtFormulaCalcShortestDuration(tprData
							.getDuration());
				}
	
			} else if (TotalSummaryProcessor.REPORT.equalsIgnoreCase(tprData
					.getType())) {
				totalSumData.incReportsTotal(tprData.getDuration());
				// set Longest and Shortest
				if (totalSumData.getReportLongestDuration() < tprData.getDuration()) {
					totalSumData.setReportLongestDuration(tprData.getDuration());
				} else if (totalSumData.getReportShortestDuration() > tprData
						.getDuration()) {
					totalSumData.setReportShortestDuration(tprData.getDuration());
				}
			} else if (TotalSummaryProcessor.SO_TRANS.equalsIgnoreCase(tprData
					.getType())) {
				totalSumData.incSOTransitionTotal(tprData.getDuration());
				// set Longest and Shortest
				if (totalSumData.getsOTransitionLongestDuration() < tprData
						.getDuration()) {
					totalSumData.setsOTransitionLongestDuration(tprData
							.getDuration());
				} else if (totalSumData.getsOTransitionShortestDuration() > tprData
						.getDuration()) {
					totalSumData.setsOTransitionShortestDuration(tprData
							.getDuration());
				}
			} else if (TotalSummaryProcessor.WORKFLOW_ASYNC
					.equalsIgnoreCase(tprData.getType())) {
				totalSumData.incWorkflowAsyncTotal(tprData.getDuration());
				// set Longest and Shortest
				if (totalSumData.getWorkflowAsyncLongestDuration() < tprData
						.getDuration()) {
					totalSumData.setWorkflowAsyncLongestDuration(tprData
							.getDuration());
				} else if (totalSumData.getWorkflowAsyncShortestDuration() > tprData
						.getDuration()) {
					totalSumData.setWorkflowAsyncShortestDuration(tprData
							.getDuration());
				}
			} else if (TotalSummaryProcessor.WORKFLOW_SYNC.equalsIgnoreCase(tprData
					.getType())) {
				totalSumData.incWorkflowSyncTotal(tprData.getDuration());
				// set Longest and Shortest
				if (totalSumData.getWorkflowSyncLongestDuration() < tprData
						.getDuration()) {
					totalSumData.setWorkflowSyncLongestDuration(tprData
							.getDuration());
				} else if (totalSumData.getWorkflowSyncShortestDuration() > tprData
						.getDuration()) {
					totalSumData.setWorkflowSyncShortestDuration(tprData
							.getDuration());
				}
	
			} else {
				totalSumData.incTotalUnknownTypes(tprData.getDuration());
			}
		}
	}

	@Override
	public Collection<?> getDataSource() {
		ArrayList<TotalSummaryData> rtnAl = new ArrayList<TotalSummaryData>();
		rtnAl.add(totalSumData);
		return rtnAl;
	}

}
