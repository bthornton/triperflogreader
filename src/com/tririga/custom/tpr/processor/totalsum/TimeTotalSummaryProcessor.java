package com.tririga.custom.tpr.processor.totalsum;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.tririga.custom.tpr.data.TprData;
import com.tririga.custom.tpr.data.TypeEnum;
import com.tririga.custom.tpr.processor.timesum.TimeRangeEnum;
import com.tririga.custom.tpr.processor.util.DataUtil;


public class TimeTotalSummaryProcessor  extends TotalSummaryProcessor {
	/** The summary. */
	private HashMap<Date, TotalSummaryProcessor> summary = new HashMap<Date, TotalSummaryProcessor>();
	private List<TypeEnum> excludeElementTypes;
	private TimeRangeEnum timeRange = null;
	
	public TimeTotalSummaryProcessor(boolean register, TimeRangeEnum timeRange, List<TypeEnum> excludeElementTypes) {
		super(register, excludeElementTypes);
		this.timeRange = timeRange;
		this.excludeElementTypes = excludeElementTypes;
	}

	@Override
	public void process(TprData tprData) throws Exception {
		if(!this.excludeElementTypes.contains(TypeEnum.getTypeEnum(tprData.getType()))) {
			DataUtil dUtil = new DataUtil();
			
			Date dateKey = dUtil.getDateKey(tprData.getStartTime(),timeRange);
			TotalSummaryProcessor activeSummary = summary.get(dateKey);
	
			if (activeSummary == null) {
				activeSummary = new TotalSummaryProcessor(false, this.excludeElementTypes);
			}
			
			activeSummary.process(tprData);
			summary.put(dateKey, activeSummary);
		}
		
	}

	@Override
	public Collection<?> getDataSource() {
		
		ArrayList<TotalSummaryData> rtnAl = new ArrayList<TotalSummaryData>();
	//	rtnAl.add(totalSumData);
		
		Collection<TotalSummaryProcessor> col = summary.values();
		for (TotalSummaryProcessor proc : col) {
			// should do a better search
			for(TotalSummaryData sumData: (Collection<? extends TotalSummaryData>)proc.getDataSource()) {
				rtnAl.add(sumData);
			}
		
		}
		
		return rtnAl;
		
	}

}
