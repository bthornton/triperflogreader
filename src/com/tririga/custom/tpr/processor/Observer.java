package com.tririga.custom.tpr.processor;

import java.util.Collection;

import com.tririga.custom.tpr.data.TprData;

// TODO: Auto-generated Javadoc
/**
 * The Class Observer.
 */
public abstract class Observer {

	/** The instance. */
	private Processor instance = Processor.getInstance();

	public Observer(boolean register) {
		if(register) {
			instance.registerObserver(this);
		}
	}

	/**
	 * Process.
	 * 
	 * @param tprData
	 *            the tpr data
	 */
	abstract public void process(TprData tprData) throws Exception;

	/**
	 * Gets the data source.
	 * 
	 * @return the data source
	 */
	abstract public Collection<?> getDataSource();

	/**
	 * Removes the observer.
	 */
	public void removeObserver() {
		instance.removeObserver(this);
	}

}
