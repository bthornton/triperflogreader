package com.tririga.custom.tpr.processor;

import com.tririga.custom.tpr.data.TprData;

public interface Subject {
	public void registerObserver(Observer observer);

	public void removeObserver(Observer observer);
	
	public void notifyObservers(TprData tprData) throws Exception;

}
