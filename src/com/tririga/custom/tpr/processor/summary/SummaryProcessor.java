package com.tririga.custom.tpr.processor.summary;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.tririga.custom.tpr.data.TprData;
import com.tririga.custom.tpr.data.TypeEnum;
import com.tririga.custom.tpr.processor.Observer;
import com.tririga.custom.tpr.processor.timesum.TimeRangeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class SummaryProcessor.
 */
public class SummaryProcessor extends Observer {

	/** The summary. */
	private HashMap<String, SummaryData> summary = new HashMap<String, SummaryData>();

	private Date dateKey = null;
	private int threashold1;
	private int threashold2;
	private List<TypeEnum> excludeElementTypes;
	private List<String> excludeElements;

	public SummaryProcessor(boolean register, int threashold1, int threashold2, List<TypeEnum> excludeElementTypes, List<String> excludeElements) {
		super(register);
		this.init(threashold1, threashold2,excludeElementTypes, excludeElements);
	}

	public SummaryProcessor(boolean register, Date dateKey, int threashold1, int threashold2, List<TypeEnum> excludeElementTypes, List<String> excludeElements, TimeRangeEnum timeRange) {
		super(false);
		this.dateKey = dateKey;
		this.init(threashold1, threashold2, excludeElementTypes, excludeElements);

	}
	
	private void init( int threashold1, int threashold2, List<TypeEnum> excludeElementTypes, List<String >excludeElements) {
		this.excludeElements = excludeElements;
		this.excludeElementTypes = excludeElementTypes;
		this.threashold1 = threashold1;
		this.threashold2 = threashold2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tririga.custom.tpr.processor.Observer#process(com.tririga.custom.
	 * tpr.data.TprData)
	 */
	@Override
	public void process(TprData tprData) throws Exception {
		if((!this.excludeElementTypes.contains(TypeEnum.getTypeEnum(tprData.getType()))) && 
		   (!excludeElements.contains(tprData.getKey().toString()))) {
			SummaryData sumData = summary.get(tprData.getKey());
			if (sumData == null) {
				sumData = setNewSumData(tprData);
			} else {
				sumData = this.updateSumData(sumData, tprData);
			}

			summary.put(sumData.getKey(), sumData);
		}

	}
	

	/**
	 * Sets the new sum data.
	 * 
	 * @param tprData
	 *            the tpr data
	 * @return the summary data
	 * @throws Exception 
	 */
	private SummaryData setNewSumData(TprData tprData) throws Exception {
		SummaryData sumData = new SummaryData();
		
		sumData.setDate(dateKey);
		sumData.setThreashold1(this.threashold1);
		sumData.setThreashold2(this.threashold2);
		
		//set the threasholds
		if(tprData.getDuration() > this.threashold1 &&
			tprData.getDuration()< this.threashold2) {
			sumData.setThreashold1Count(sumData.getThreashold1Count()+1);
		} else if (tprData.getDuration() > this.threashold2) {
			sumData.setThreashold2Count(sumData.getThreashold2Count()+1);
		}
		
		sumData.setKey(tprData.getKey());
		sumData.setType(tprData.getType());
		sumData.setTotalCount(1);
		sumData.setMaxVal(tprData.getDuration());
		sumData.setMinVal(tprData.getDuration());
		sumData.setTotalDuration(tprData.getDuration());
		sumData.setMaxTprdata(tprData);
		sumData.setMinTprData(tprData);

		return sumData;
	}

	/**
	 * Update sum data.
	 * 
	 * @param sumData
	 *            the sum data
	 * @param tprData
	 *            the tpr data
	 * @return the summary data
	 */
	protected SummaryData updateSumData(SummaryData sumData, TprData tprData) {
		sumData.setTotalCount(sumData.getTotalCount() + 1);

		if (sumData.getMaxVal() < tprData.getDuration()) {
			sumData.setMaxVal(tprData.getDuration());
			sumData.setMaxTprdata(tprData);

		}

		if (sumData.getMinVal() > tprData.getDuration()) {
			sumData.setMinVal(tprData.getDuration());
			sumData.setMinTprData(tprData);

		}
		
		//set the threasholds
		if(tprData.getDuration() > this.threashold1 &&
			tprData.getDuration()< this.threashold2) {
			sumData.setThreashold1Count(sumData.getThreashold1Count()+1);
		} else if (tprData.getDuration() > this.threashold2) {
			sumData.setThreashold2Count(sumData.getThreashold2Count()+1);
		}

		sumData.setTotalDuration(sumData.getTotalDuration()
				+ tprData.getDuration());

		return sumData;
	}

	public Date getDateKey() {
		return dateKey;
	}

	public void setDateKey(Date dateKey) {
		this.dateKey = dateKey;
	}

	@Override
	public Collection<?> getDataSource() {

		return summary.values();

	}

}
