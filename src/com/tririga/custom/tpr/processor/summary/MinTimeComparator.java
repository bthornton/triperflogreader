package com.tririga.custom.tpr.processor.summary;

import java.util.Comparator;

public class MinTimeComparator implements Comparator<SummaryData> {

	@Override
	public int compare(SummaryData arg0, SummaryData arg1) {

		return (int) (arg0.getMinVal() - arg1.getMinVal());

	}

}
