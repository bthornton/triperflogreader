/*
 * 
 */
package com.tririga.custom.tpr.processor.summary;

// TODO: Auto-generated Javadoc
/**
 * The Class MinMaxData.
 */
public class MinMaxData {

	/** The type. */
	private String type;
	/** The key. */
	private String key;

	/** The type. */
	private String lineType;

	/** The val. */
	private long val;

	/** The details. */
	private String details;

	/**
	 * Gets the key.
	 * 
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 * 
	 * @param key
	 *            the new key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getLineType() {
		return lineType;
	}

	/**
	 * Sets the type.
	 * 
	 * @param lineType
	 *            the new line type
	 */
	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	/**
	 * Gets the val.
	 * 
	 * @return the val
	 */
	public long getVal() {
		return val;
	}

	/**
	 * Sets the val.
	 * 
	 * @param val
	 *            the new val
	 */
	public void setVal(long val) {
		this.val = val;
	}

	/**
	 * Gets the details.
	 * 
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * Sets the details.
	 * 
	 * @param details
	 *            the new details
	 */
	public void setDetails(String details) {
		this.details = details;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "MinMaxData [type=" + type + ", key=" + key + ", lineType="
				+ lineType + ", val=" + val + ", details=" + details + "]";
	}

}
