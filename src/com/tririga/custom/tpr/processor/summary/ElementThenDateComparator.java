package com.tririga.custom.tpr.processor.summary;

import java.text.SimpleDateFormat;
import java.util.Comparator;

public class ElementThenDateComparator implements Comparator<SummaryData> {

	@Override
	public int compare(SummaryData arg0, SummaryData arg1) {
		  if (arg0 == arg1) {
		        return 0;
		    }
		    if (arg0 == null) {
		        return -1;
		    }
		    if (arg1 == null) {
		        return 1;
		    }
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    String arg0Comp = arg0.getKey()+sdf.format(arg0.getDate());
		    String arg1Comp = arg1.getKey()+sdf.format(arg1.getDate());
		    return arg0Comp.compareTo(arg1Comp);


	}

}
