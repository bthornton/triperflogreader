package com.tririga.custom.tpr.processor.summary;

import java.util.Date;

import com.tririga.custom.tpr.data.TprData;
import com.tririga.custom.tpr.reports.DataSourceI;

// TODO: Auto-generated Javadoc
/**
 * The Class SummaryData.
 */
public class SummaryData implements DataSourceI {

	
	private int threashold1;
	private int threashold2;
	
	private int threashold1Count;
	private int threashold2Count;
	private int index;

	private Date date;

	/** The key. */
	private String key = "";

	/** The type. */
	private String type = "";

	/** The min val. */
	private int minVal = 0;

	/** The max val. */
	private int maxVal = 0;

	/** The total count. */
	private int totalCount = 0;

	/** The total duration. */
	private int totalDuration = 0;

	/** The min tpr data. */
	private TprData minTprData = null;

	/** The max tprdata. */
	private TprData maxTprdata = null;
	
	
	public int getApprovedCount() {
		return this.getTotalCount()-this.getThreashold1Count() - this.getThreashold2Count();
	}
	
	public int getThreashold1() {
		return threashold1;
	}

	public void setThreashold1(int threashold1) {
		this.threashold1 = threashold1;
	}

	public int getThreashold2() {
		return threashold2;
	}

	public void setThreashold2(int threashold2) {
		this.threashold2 = threashold2;
	}

	public int getThreashold1Count() {
		return threashold1Count;
	}

	public void setThreashold1Count(int threashold1Count) {
		this.threashold1Count = threashold1Count;
	}

	public int getThreashold2Count() {
		return threashold2Count;
	}

	public void setThreashold2Count(int threashold2Count) {
		this.threashold2Count = threashold2Count;
	}

	public int getIndex() {
		return index;
	}

	public String getStringIndex() {
		return "" + index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public TprData getMaxTprdata() {
		return maxTprdata;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Gets the key.
	 * 
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 * 
	 * @param key
	 *            the new key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Gets the min val.
	 * 
	 * @return the min val
	 */
	public int getMinVal() {
		return minVal;
	}

	/**
	 * Sets the min val.
	 * 
	 * @param minVal
	 *            the new min val
	 */
	public void setMinVal(int minVal) {
		this.minVal = minVal;
	}

	/**
	 * Gets the max val.
	 * 
	 * @return the max val
	 */
	public int getMaxVal() {
		return maxVal;
	}


	/**
	 * Sets the max val.
	 * 
	 * @param maxVal
	 *            the new max val
	 */
	public void setMaxVal(int maxVal) {
		this.maxVal = maxVal;
	}

	/**
	 * Gets the avg val.
	 * 
	 * @return the avg val
	 */
	public int getAvgVal() {
		return this.totalDuration / this.totalCount;
	}



	/**
	 * Gets the total count.
	 * 
	 * @return the total count
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * Sets the total count.
	 * 
	 * @param totalCount
	 *            the new total count
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * Gets the total duration.
	 * 
	 * @return the total duration
	 */
	public int getTotalDuration() {
		return totalDuration;
	}



	/**
	 * Sets the total duration.
	 * 
	 * @param totalDuration
	 *            the new total duration
	 */
	public void setTotalDuration(int totalDuration) {
		this.totalDuration = totalDuration;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the min tpr data.
	 * 
	 * @return the min tpr data
	 */
	public TprData getMinTprData() {
		return minTprData;
	}

	/**
	 * Sets the min tpr data.
	 * 
	 * @param minTprData
	 *            the new min tpr data
	 */
	public void setMinTprData(TprData minTprData) {
		this.minTprData = minTprData;
	}

	/**
	 * Gets the max tprdata.
	 * 
	 * @return the max tprdata
	 */
	public TprData getMaxTprData() {
		return maxTprdata;
	}

	/**
	 * Sets the max tprdata.
	 * 
	 * @param maxTprdata
	 *            the new max tprdata
	 */
	public void setMaxTprdata(TprData maxTprdata) {
		this.maxTprdata = maxTprdata;
	}

	@Override
	public String toString() {
		return "SummaryData [index=" + index + ", date=" + date + ", key="
				+ key + ", type=" + type + ", minVal=" + minVal + ", maxVal="
				+ maxVal + ", totalCount=" + totalCount + ", totalDuration="
				+ totalDuration + ", minTprData=" + minTprData
				+ ", maxTprdata=" + maxTprdata + "]";
	}

	

}
