package com.tririga.custom.tpr.processor.summary;

public class MinData extends MinMaxData {
	public static final String TYPE = "Min";

	public MinData() {
		this.setLineType(MinData.TYPE);
	}
}
