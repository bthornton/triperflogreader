package com.tririga.custom.tpr.processor.summary;

import java.util.Comparator;

public class Threashold2Comparator implements Comparator<SummaryData> {

	@Override
	public int compare(SummaryData arg0, SummaryData arg1) {

		int rtnVal = 0;
		
		
		if((arg0.getThreashold2Count() - arg1.getThreashold2Count())  < 0 ) {
			rtnVal = -1;
		} else if ((arg0.getThreashold2Count() - arg1.getThreashold2Count())  > 0 ) {
			rtnVal = 1;
		} else if (arg0.getThreashold1Count() - arg1.getThreashold1Count() < 0) {
			// threashold2 are the same
			rtnVal = -1;
		} else if (arg0.getThreashold1Count() - arg1.getThreashold1Count() > 0) {
			rtnVal = 1;
		} else if(arg0.getApprovedCount() - arg1.getApprovedCount() < 0) {
			rtnVal = -1;
		} else if(arg0.getApprovedCount() - arg1.getApprovedCount() > 0) {
			rtnVal = 1;
		}		

		return rtnVal;
	}

}
