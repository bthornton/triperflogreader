package com.tririga.custom.tpr.processor.summary;

public class MaxData extends MinMaxData {

	public static final String TYPE = "Max";

	public MaxData() {
		this.setLineType(MaxData.TYPE);
	}
}
