package com.tririga.custom.tpr.processor.summary;

import java.util.Comparator;

public class ElementComparator implements Comparator<SummaryData> {

	@Override
	public int compare(SummaryData arg0, SummaryData arg1) {
		  if (arg0 == arg1) {
		        return 0;
		    }
		    if (arg0 == null) {
		        return -1;
		    }
		    if (arg1 == null) {
		        return 1;
		    }

		    return arg0.getKey().compareTo(arg1.getKey());

	}

}
