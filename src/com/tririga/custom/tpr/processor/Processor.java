package com.tririga.custom.tpr.processor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.tririga.custom.tpr.data.TprData;

public class Processor implements Subject {
	private List<Observer> observers = Collections.synchronizedList(new ArrayList<Observer>());

	private static Processor instance = null;

	private Processor() {

	}

	public static Processor getInstance() {
		if (instance == null) {
			instance = new Processor();
		}
		return instance;
	}

	@Override
	public void registerObserver(Observer observer) {
		observers.add(observer);

	}

	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);

	}

	@Override
	public void notifyObservers(TprData tprData) throws Exception {
		for (Observer obs : observers) {
			obs.process(tprData);
		}

	}


}
