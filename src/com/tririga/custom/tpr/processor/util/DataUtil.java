package com.tririga.custom.tpr.processor.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.tririga.custom.tpr.processor.timesum.TimeRangeEnum;

public class DataUtil {
	public Date getDateKey(long time, TimeRangeEnum timeFrame) throws Exception {
		
		
		return new SimpleDateFormat("yy:M:d:k", Locale.ENGLISH).parse(getDateString(time, timeFrame));
	}

	
	public String getDateString(long time, TimeRangeEnum timeFrame) throws Exception {
		String dateKey = null;

		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(time);

		switch (timeFrame) {
		case WEEK:
			dateKey = "" + cal.get(Calendar.YEAR) +":"+ (cal.get(Calendar.MONTH)+1)
					+":"+ cal.get(Calendar.DAY_OF_MONTH)+":1";
			break;
		case DAY:
			dateKey = "" + cal.get(Calendar.YEAR) +":"+ (cal.get(Calendar.MONTH)+1)
					+":"+ cal.get(Calendar.DAY_OF_MONTH)+":1";
//			dateKey = "1:1:"+ cal.get(Calendar.DAY_OF_MONTH)+":1";
			break;
		case HOUR:
	//		dateKey = "" + cal.get(Calendar.YEAR) +":"+ cal.get(Calendar.MONTH)
	//				+":"+ cal.get(Calendar.DAY_OF_MONTH)+":"+cal.get(Calendar.HOUR_OF_DAY);
			dateKey = "1:1:1:" +cal.get(Calendar.HOUR_OF_DAY);
	//		System.out.println(dateKey);
			break;
		case DAY_NO_HOUR:
//			dateKey = "" + cal.get(Calendar.YEAR) +":"+ cal.get(Calendar.MONTH)
//			+":"+cal.get(Calendar.DAY_OF_MONTH);
			dateKey = "1:1:1";
			break;
		default:
			dateKey = "" + cal.get(Calendar.YEAR) +":"+ cal.get(Calendar.MONTH)
					+":"+ cal.get(Calendar.DAY_OF_MONTH)+":1";
			break;
		}
		
		return dateKey;
		
	}
	
}
