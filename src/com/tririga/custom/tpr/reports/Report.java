package com.tririga.custom.tpr.reports;

public interface Report {

	public void configFirstObservers();

	public void configSecondObservers();

	public void runReport() throws Exception;

}
