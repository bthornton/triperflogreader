package com.tririga.custom.tpr.reports.system;

import java.util.Collection;

import com.tririga.custom.tpr.processor.summary.SummaryData;

public interface SummaryStore {

	public void store(Collection<SummaryData> col) throws Exception;

}
