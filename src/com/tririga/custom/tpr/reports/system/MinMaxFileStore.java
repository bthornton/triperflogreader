package com.tririga.custom.tpr.reports.system;

import java.io.FileWriter;
import java.util.Collection;

import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.tririga.custom.tpr.processor.summary.MaxData;
import com.tririga.custom.tpr.processor.summary.MinData;
import com.tririga.custom.tpr.processor.summary.MinMaxData;
import com.tririga.custom.tpr.processor.summary.SummaryData;
import com.tririga.custom.tpr.reports.DataSourceI;

// TODO: Auto-generated Javadoc
/**
 * The Class MinMaxFileStore.
 */
public class MinMaxFileStore implements SummaryStore {
	private static final String outputDir = "output";

	/** The output file name. */
	private String outputFileName = "temp.txt";

	/** The sum processors. */
	private final CellProcessor[] sum_processors = new CellProcessor[] {
			new NotNull(), // key
			new NotNull(), // type (Min or Max)
			new ParseLong(), // Val
			new NotNull() }; // Details

	/** The header. */
	private final String[] header = new String[] { "key", "type", "val",
			"details" };

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tririga.custom.tpr.processor.summary.SummaryStore#store(java.util
	 * .Collection)
	 */
	public void store(Collection<SummaryData> col) throws Exception {
		System.out.println();
		System.out.println("FileStore - TprData....");
		ICsvBeanWriter beanWriter = null;

		try {
			beanWriter = new CsvBeanWriter(new FileWriter(outputDir + "/"
					+ outputFileName), CsvPreference.STANDARD_PREFERENCE);

			beanWriter.writeHeader(header);

			// write the beans
			for (final DataSourceI sumData : col) {
				beanWriter.write(getMinData(sumData), header, sum_processors);
				beanWriter.write(getMaxData(sumData), header, sum_processors);
			}
		} finally {
			if (beanWriter != null) {
				beanWriter.close();
			}
		}
	}

	/**
	 * Gets the min data.
	 * 
	 * @param data
	 *            the data
	 * @return the min data
	 */
	private MinMaxData getMinData(DataSourceI data) {
		MinData minData = new MinData();
		if (data instanceof SummaryData) {
			SummaryData sumData = (SummaryData) data;

			minData.setKey(sumData.getKey());
			minData.setType(sumData.getType());
			minData.setVal(sumData.getMinVal());
			minData.setDetails(sumData.getMinTprData().toString());
		}
		return minData;
	}

	/**
	 * Gets the max data.
	 * 
	 * @param data
	 *            the data
	 * @return the max data
	 */
	private MinMaxData getMaxData(DataSourceI data) {
		MaxData maxData = new MaxData();
		if (data instanceof SummaryData) {
			SummaryData sumData = (SummaryData) data;

			maxData.setKey(sumData.getKey());
			maxData.setType(sumData.getType());
			maxData.setVal(sumData.getMaxVal());
			maxData.setDetails(sumData.getMaxTprData().toString());
		}
		return maxData;
	}

	/**
	 * Gets the output file name.
	 * 
	 * @return the output file name
	 */
	public String getOutputFileName() {
		return outputFileName;
	}

	/**
	 * Sets the output file name.
	 * 
	 * @param outputFileName
	 *            the new output file name
	 */
	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}

}
