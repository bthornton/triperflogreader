package com.tririga.custom.tpr.reports.system;

import java.util.Collection;

import com.tririga.custom.tpr.processor.summary.SummaryData;
import com.tririga.custom.tpr.reports.DataSourceI;

public class MinMaxOutStore implements SummaryStore {

	@Override
	public void store(Collection<SummaryData> col) throws Exception {

		for (DataSourceI data : col) {
			if (data instanceof SummaryData) {
				SummaryData sumData = (SummaryData) data;
				System.out.println("key=" + sumData.getKey() + ", Min="
						+ sumData.getMinVal() + ", " + sumData.getMinTprData());
				System.out.println("key=" + sumData.getKey() + ", Max="
						+ sumData.getMaxVal() + ", " + sumData.getMaxTprData());
			}
		}
	}

}
