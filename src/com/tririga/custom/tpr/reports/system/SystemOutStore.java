package com.tririga.custom.tpr.reports.system;

import java.util.Collection;

import com.tririga.custom.tpr.processor.summary.SummaryData;
import com.tririga.custom.tpr.reports.DataSourceI;

// TODO: Auto-generated Javadoc
/**
 * The Class FileStore.
 */
public class SystemOutStore implements SummaryStore {

	/**
	 * Store.
	 */
	public void store(Collection<SummaryData> col) {

		for (DataSourceI sumData : col) {
			System.out.println(sumData);
		}

	}

}
