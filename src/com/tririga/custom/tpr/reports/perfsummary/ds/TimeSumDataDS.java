package com.tririga.custom.tpr.reports.perfsummary.ds;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;

import com.tririga.custom.tpr.processor.summary.AvgTimeComparator;
import com.tririga.custom.tpr.processor.summary.CountComparator;
import com.tririga.custom.tpr.processor.summary.DurationComparator;
import com.tririga.custom.tpr.processor.summary.ElementThenDateComparator;
import com.tririga.custom.tpr.processor.summary.MaxTimeComparator;
import com.tririga.custom.tpr.processor.summary.MinTimeComparator;
import com.tririga.custom.tpr.processor.summary.SummaryData;
import com.tririga.custom.tpr.processor.timesum.TimeSummaryProcessor;
import com.tririga.custom.tpr.processor.totalsum.TotalSummaryData;
import com.tririga.custom.tpr.reports.DataSourceI;
import com.tririga.custom.tpr.reports.perfsummary.ReportUtil;
import com.tririga.custom.tpr.reports.perfsummary.SumDataSortOrder;

// TODO: Auto-generated Javadoc
/**
 * The Class TimeSumDataDS.
 */
public class TimeSumDataDS extends SumDataReportDS {

	
	public static final String STRING_INDEX = "stringIndex";
	
	/**
	 * Creates the sum data ds.
	 *
	 * @param timeSumProc the time sum proc
	 * @param sortOrder the sort order
	 * @param topN            the top n
	 * @return the JR data source
	 */
	public JRDataSource createTimeSumDataDS(TimeSummaryProcessor timeSumProc,TotalSummaryData totalSumdata,
			SumDataSortOrder sortOrder, int topN) {
		/** The Constant ELEMENT_CM. */
		
		ReportUtil repUtil = new ReportUtil();

		
		DRDataSource dataSource = new DRDataSource(TimeSumDataDS.DATE_CM,
				TimeSumDataDS.HH_STR_CM, 
				TimeSumDataDS.DAY_STR_CM, 
				TimeSumDataDS.STRING_INDEX, 
				TimeSumDataDS.ELEMENT_CM,
				TimeSumDataDS.TYPE_CM, 
				TimeSumDataDS.MIN_VAL_CM,
				TimeSumDataDS.MAX_VAL_CM, 
				TimeSumDataDS.AVG_VAL_CM, 
				TimeSumDataDS.DUR_VAL_CM, 
				TimeSumDataDS.COUNT_VAL_CM,
				TimeSumDataDS.APPROVED_COUNT_CM,
				TimeSumDataDS.THRESHOLD_COUNT_1_CM,
				TimeSumDataDS.THRESHOLD_COUNT_2_CM);

		List<SummaryData> dataList = null;

		switch (sortOrder) {
		case MIN:
			dataList = getTimeSumDataSort(timeSumProc, topN, new MinTimeComparator());
			break;
		case MAX:
			dataList = getTimeSumDataSort(timeSumProc, topN, new MaxTimeComparator());
			break;
		case AVG:
			dataList = getTimeSumDataSort(timeSumProc, topN, new AvgTimeComparator());
			break;
		case DURATION:
			dataList = getTimeSumDataSort(timeSumProc, topN, new DurationComparator());
			break;
		case COUNT:
			dataList = getTimeSumDataSort(timeSumProc, topN, new CountComparator());
			break;
		case ELEMENT:
			dataList = getTimeSumDataSort(timeSumProc, topN, new ElementThenDateComparator());
			// decending
			Collections.reverse(dataList);
	
			break;
		default:
			dataList = getTimeSumDataSort(timeSumProc, topN, new DurationComparator());
			break;

		}
		DateFormat dfHour = new SimpleDateFormat("HH");
		DateFormat dfDay = new SimpleDateFormat("yyyy-MM-dd");

		
		
		
		// pad data set days
		Date startDate = new Date(totalSumdata.getFirstRecord().getStartTime());
//		System.out.println(dfDay.format(startDate));
		// This pads way too much and should be optimized
		for (SummaryData data : dataList) {
			for(int i = 1; i < totalSumdata.getReportDurationDays(); i++) {
//				System.out.println(dfDay.format(addDays(startDate, i)));
				dataSource.add(data.getDate(), "00", dfDay.format(addDays(startDate, i)),
						data.getStringIndex(), data.getKey(),
						data.getType(), 
						0.0,
						0.0,
						0.0,
						0.0,
						0,
						0,
						0,
						0);	
			}
			for(int i = 0; i < 24; i++) {
				//buffer for hour
				String tmpDate = "";
				if( i < 10) {
					tmpDate = "0"+i;
				} else {
					tmpDate = ""+i;
				}
				System.out.println("--"+dfDay.format(data.getDate()));
				dataSource.add(data.getDate(), tmpDate, dfDay.format(data.getDate()),
						data.getStringIndex(), data.getKey(),
						data.getType(), 
						0.0,
						0.0,
						0.0,
						0.0,
						0,
						0,
						0,
						0);
				
			}
			dataSource.add(data.getDate(), dfHour.format(data.getDate()), 
					dfDay.format(data.getDate()),
					data.getStringIndex(), data.getKey(),
					data.getType(), 
					repUtil.getMilliToSec(data.getMinVal()),
					repUtil.getMilliToSec(data.getMaxVal()),
					repUtil.getMilliToSec(data.getAvgVal()),
					repUtil.getMilliToSec(data.getTotalDuration()),
					data.getTotalCount(),
					data.getApprovedCount(),
					data.getThreashold1Count(),
					data.getThreashold2Count());

		}
		return dataSource;
	}
	
	 public static Date addDays(Date date, int days)
	    {
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(date);
	        cal.add(Calendar.DATE, days); //minus number would decrement the days
	        return cal.getTime();
	    }

	
	/**
	 * Gets the sum data sorted max time.
	 *
	 * @param timeSumProc the time sum proc
	 * @param topN            the top n
	 * @param compare the compare
	 * @return the sum data sorted max time
	 */
	public List<SummaryData> getTimeSumDataSort(TimeSummaryProcessor timeSumProc, int topN,
			Comparator<SummaryData> compare) {

		@SuppressWarnings("unchecked")
		Collection<DataSourceI> dataSource = (Collection<DataSourceI>) timeSumProc
				.getDataSource();

		List<DataSourceI> dataSourceList = new ArrayList<DataSourceI>(
				dataSource);
		List<SummaryData> sortedList = new ArrayList<SummaryData>();

		if (topN > dataSourceList.size()) {
			topN = dataSourceList.size();
		}

		// convert totalList and set the index
		for (int i = 0; i < dataSourceList.size(); i++) {
			SummaryData sumData = (SummaryData) dataSourceList.get(i);
			//sumData.setIndex(i + 1);
			sortedList.add(sumData);

		}

		Collections.sort(sortedList, compare);

		// decending
		Collections.reverse(sortedList);
		return sortedList.subList(0, topN);

	}
	
	public List<SummaryData> bufferHours(List<SummaryData> data) {
		
		
		return data;
	}
	
	
}
