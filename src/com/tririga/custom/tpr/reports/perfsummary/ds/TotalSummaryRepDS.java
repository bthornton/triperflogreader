package com.tririga.custom.tpr.reports.perfsummary.ds;

import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;

import com.tririga.custom.tpr.processor.totalsum.TotalSummaryData;
import com.tririga.custom.tpr.processor.totalsum.TotalSummaryProcessor;
import com.tririga.custom.tpr.reports.DataSourceI;
import com.tririga.custom.tpr.reports.perfsummary.ReportUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class TotalSummaryRepDS.
 */
public class TotalSummaryRepDS {
	
	/** The Constant ELEMENT_CM. */
	public static final String DATE_CM = "Date";

	/** The Constant ELEMENT_CM. */
	public static final String ELEMENT_CM = "element";
	
	/** The Constant COUNT_CM. */
	public static final String COUNT_CM = "count";
	
	/** The Constant DUR_CM. */
	public static final String DUR_CM = "toalDur";
	
	/** The Constant DUR_LONGEST_CM. */
	public static final String DUR_LONGEST_CM = "longestDur";
	
	/** The Constant DUR_SHORTEST_CM. */
	public static final String DUR_SHORTEST_CM = "shortestDur";
	
	/**
	 * Creates the total summary ds.
	 *
	 * @param totalSumProc the total sum proc
	 * @return the JR data source
	 */
	public JRDataSource createTotalSummaryDS(TotalSummaryProcessor totalSumProc) {

		DRDataSource dataSource = new DRDataSource(TotalSummaryRepDS.ELEMENT_CM, 
				TotalSummaryRepDS.COUNT_CM,
				TotalSummaryRepDS.DUR_CM,
				TotalSummaryRepDS.DUR_LONGEST_CM,
				TotalSummaryRepDS.DUR_SHORTEST_CM);

		TotalSummaryData data = getTotalSummaryData(totalSumProc);
		ReportUtil repUtil = new ReportUtil();

		dataSource.add("ExtF", data.getExtFormulaCalcTotal(),
				repUtil.getMilliToMin(data.getExtFormulaCalcDurTotal()),
				repUtil.getMilliToMin(data.getExtFormulaCalcLongestDuration()),
				repUtil.getMilliToMin(data.getExtFormulaCalcShortestDuration()));

		dataSource.add("ExtF Q", data.getExtFormulaQueueTotal(),
				repUtil.getMilliToMin(data.getExtFormulaQueueDurTotal()),
				repUtil.getMilliToMin(data.getExtFormulaQueueLongestDuration()),
				repUtil.getMilliToMin(data.getExtFormulaQueueShortestDuration()));

		dataSource.add("Reports", data.getReportsTotal(),
				repUtil.getMilliToMin(data.getReportsDurTotal()),
				repUtil.getMilliToMin(data.getReportLongestDuration()),
				repUtil.getMilliToMin(data.getReportShortestDuration()));
				
		dataSource.add("SO", data.getsOTransitionTotal(),
				repUtil.getMilliToMin(data.getsOTransitionDurTotal()),
				repUtil.getMilliToMin(data.getsOTransitionLongestDuration()),
				repUtil.getMilliToMin(data.getsOTransitionShortestDuration()));

		dataSource.add("WFAsync", data.getWorkflowAsyncTotal(),
				repUtil.getMilliToMin(data.getWorkflowAsyncDurTotal()),
				repUtil.getMilliToMin(data.getWorkflowAsyncLongestDuration()),
				repUtil.getMilliToMin(data.getWorkflowAsyncShortestDuration()));
		
		dataSource.add("WFSync", data.getWorkflowSyncTotal(),
				repUtil.getMilliToMin(data.getWorkflowSyncDurTotal()),
				repUtil.getMilliToMin(data.getWorkflowSyncLongestDuration()),
				repUtil.getMilliToMin(data.getWorkflowSyncShortestDuration()));

		return dataSource;
	}
	
	/**
	 * Creates the total summary ds.
	 *
	 * @param totalSumProc the total sum proc
	 * @return the JR data source
	 */
	public JRDataSource createTimeTotalSummaryDS(TotalSummaryProcessor totalSumProc) {

		DRDataSource dataSource = new DRDataSource(TotalSummaryRepDS.DATE_CM,
//				TotalSummaryRepDS.ELEMENT_CM, 
				TotalSummaryRepDS.COUNT_CM,
				TotalSummaryRepDS.DUR_CM,
				TotalSummaryRepDS.DUR_LONGEST_CM,
				TotalSummaryRepDS.DUR_SHORTEST_CM);
		DateFormat dfDay = new SimpleDateFormat("yyyy-MM-dd");
		for(TotalSummaryData data: getTimeTotalSummaryData(totalSumProc)) {
			ReportUtil repUtil = new ReportUtil();

			dataSource.add(dfDay.format(data.getFirstRecord().getStartTime()),data.getExtFormulaCalcTotal(),
					repUtil.getMilliToMin(data.getExtFormulaCalcDurTotal()),
					repUtil.getMilliToMin(data.getExtFormulaCalcLongestDuration()),
					repUtil.getMilliToMin(data.getExtFormulaCalcShortestDuration()));

			dataSource.add(dfDay.format(data.getFirstRecord().getStartTime()), data.getExtFormulaQueueTotal(),
					repUtil.getMilliToMin(data.getExtFormulaQueueDurTotal()),
					repUtil.getMilliToMin(data.getExtFormulaQueueLongestDuration()),
					repUtil.getMilliToMin(data.getExtFormulaQueueShortestDuration()));

			dataSource.add(dfDay.format(data.getFirstRecord().getStartTime()), data.getReportsTotal(),
					repUtil.getMilliToMin(data.getReportsDurTotal()),
					repUtil.getMilliToMin(data.getReportLongestDuration()),
					repUtil.getMilliToMin(data.getReportShortestDuration()));
					
			dataSource.add(dfDay.format(data.getFirstRecord().getStartTime()), data.getsOTransitionTotal(),
					repUtil.getMilliToMin(data.getsOTransitionDurTotal()),
					repUtil.getMilliToMin(data.getsOTransitionLongestDuration()),
					repUtil.getMilliToMin(data.getsOTransitionShortestDuration()));

			dataSource.add(dfDay.format(data.getFirstRecord().getStartTime()), data.getWorkflowAsyncTotal(),
					repUtil.getMilliToMin(data.getWorkflowAsyncDurTotal()),
					repUtil.getMilliToMin(data.getWorkflowAsyncLongestDuration()),
					repUtil.getMilliToMin(data.getWorkflowAsyncShortestDuration()));
			
			dataSource.add(dfDay.format(data.getFirstRecord().getStartTime()), data.getWorkflowSyncTotal(),
					repUtil.getMilliToMin(data.getWorkflowSyncDurTotal()),
					repUtil.getMilliToMin(data.getWorkflowSyncLongestDuration()),
					repUtil.getMilliToMin(data.getWorkflowSyncShortestDuration()));
			
		}

		return dataSource;
	}

	/**
	 * Gets the total summary data.
	 *
	 * @param totalSumProc the total sum proc
	 * @return the total summary data
	 */
	public TotalSummaryData getTotalSummaryData(
			TotalSummaryProcessor totalSumProc) {
		TotalSummaryData data = null;
		@SuppressWarnings("unchecked")
		Collection<DataSourceI> totalSource = (Collection<DataSourceI>) totalSumProc
				.getDataSource();
		if (totalSource.size() == 1) { 
			data = (TotalSummaryData) totalSource.iterator().next();
		}

		return data;
	}
	
	/**
	 * Gets the total summary data.
	 *
	 * @param totalSumProc the total sum proc
	 * @return the total summary data
	 */
	public List<TotalSummaryData> getTimeTotalSummaryData( TotalSummaryProcessor totalSumProc) {

		List<TotalSummaryData> rtnVal = new ArrayList<TotalSummaryData>();
		@SuppressWarnings("unchecked")
		Collection<DataSourceI> totalSource = (Collection<DataSourceI>) totalSumProc
				.getDataSource();
		for(DataSourceI tmp : totalSource) {
			rtnVal.add((TotalSummaryData) tmp);
		}

		return rtnVal;
	}
	
	/**
	 * Gets the item column.
	 *
	 * @param columnTitle the column title
	 * @return the item column
	 */
	public TextColumnBuilder<String> getItemColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalSummaryRepDS.ELEMENT_CM,
				type.stringType());
	}
	
	/**
	 * Gets the item column.
	 *
	 * @param columnTitle the column title
	 * @return the item column
	 */
	public TextColumnBuilder<String> getStartDateColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalSummaryRepDS.DATE_CM,
				type.stringType());
	}
	
	/**
	 * Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getCountColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalSummaryRepDS.COUNT_CM,
				type.integerType());
	}
	

	/**
	 * Gets the dur column.
	 *
	 * @param columnTitle the column title
	 * @return the dur column
	 */
	public TextColumnBuilder<Double> getDurColumn(String columnTitle) {
		return col.column("Duration",
			TotalSummaryRepDS.DUR_CM, 
			type.doubleType()).setPattern("##,##0.00");
	}
	
	/**
	 * Gets the dur longest column.
	 *
	 * @param columnTitle the column title
	 * @return the dur longest column
	 */
	public TextColumnBuilder<Double> getDurLongestColumn(String columnTitle) {
		return col.column(columnTitle,
			TotalSummaryRepDS.DUR_LONGEST_CM, 
			type.doubleType()).setPattern("##,##0.00");
	}
	
	/**
	 * Gets the dur shortest column.
	 *
	 * @param columnTitle the column title
	 * @return the dur shortest column
	 */
	public TextColumnBuilder<Double> getDurShortestColumn(String columnTitle) {
		return col.column(columnTitle,
			TotalSummaryRepDS.DUR_SHORTEST_CM, 
			type.doubleType()).setPattern("##,##0.00");
	}

}
