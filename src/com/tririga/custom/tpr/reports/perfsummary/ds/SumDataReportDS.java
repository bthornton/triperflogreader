package com.tririga.custom.tpr.reports.perfsummary.ds;

import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;

import com.tririga.custom.tpr.data.TypeEnum;
import com.tririga.custom.tpr.processor.summary.AvgTimeComparator;
import com.tririga.custom.tpr.processor.summary.CountComparator;
import com.tririga.custom.tpr.processor.summary.DurationComparator;
import com.tririga.custom.tpr.processor.summary.MaxTimeComparator;
import com.tririga.custom.tpr.processor.summary.MinTimeComparator;
import com.tririga.custom.tpr.processor.summary.SummaryData;
import com.tririga.custom.tpr.processor.summary.SummaryProcessor;
import com.tririga.custom.tpr.processor.summary.Threashold2Comparator;
import com.tririga.custom.tpr.reports.DataSourceI;
import com.tririga.custom.tpr.reports.perfsummary.ReportUtil;
import com.tririga.custom.tpr.reports.perfsummary.SumDataSortOrder;

// TODO: Auto-generated Javadoc
/**
 * The Class SumDataReportDS.
 */
public class SumDataReportDS {
	
	/** The Constant ELEMENT_CM. */
	public static final String DATE_CM = "date";
	
	/** The Constant HH_STR_CM. */
	public static final String HH_STR_CM = "hhStr";
	
	/** The Constant HH_STR_CM. */
	public static final String DAY_STR_CM = "dayStr";

	/** The Constant INDEX_CM. */
	public static final String INDEX_CM = "stringIndex";
	
	/** The Constant ELEMENT_CM. */
	public static final String ELEMENT_CM = "element";
	
	/** The Constant TYPE_CM. */
	public static final String TYPE_CM = "type";
	
	/** The Constant MIN_VAL_CM. */
	public static final String MIN_VAL_CM = "minVal";
	
	/** The Constant MAX_VAL_CM. */
	public static final String MAX_VAL_CM = "maxVal";
	
	/** The Constant AVG_VAL_CM. */
	public static final String AVG_VAL_CM = "avgVal";
	
	/** The Constant DUR_VAL_CM. */
	public static final String DUR_VAL_CM = "duration";
	
	/** The Constant COUNT_VAL_CM. */
	public static final String COUNT_VAL_CM = "countTotal";
	
	
	public static final String THRESHOLD_COUNT_1_CM = "threasholdcount1";
	public static final String THRESHOLD_COUNT_2_CM = "threasholdcount2";
	public static final String APPROVED_COUNT_CM = "approvedcount";

	/**
	 * Creates the sum data ds.
	 *
	 * @param sumProc the sum proc
	 * @param sortOrder the sort order
	 * @param topN            the top n
	 * @return the JR data source
	 */
	public JRDataSource createSumDataDS(SummaryProcessor sumProc,
			SumDataSortOrder sortOrder, int topN) {

		ReportUtil repUtil = new ReportUtil();

		DRDataSource dataSource = new DRDataSource(SumDataReportDS.INDEX_CM,
				SumDataReportDS.ELEMENT_CM,
				SumDataReportDS.TYPE_CM,
				SumDataReportDS.MIN_VAL_CM,
				SumDataReportDS.MAX_VAL_CM,
				SumDataReportDS.AVG_VAL_CM,
				SumDataReportDS.DUR_VAL_CM,
				SumDataReportDS.COUNT_VAL_CM,
				SumDataReportDS.APPROVED_COUNT_CM,
				SumDataReportDS.THRESHOLD_COUNT_1_CM,
				SumDataReportDS.THRESHOLD_COUNT_2_CM);

		List<SummaryData> dataList = null;

		switch (sortOrder) {
		case MIN:
			dataList = getSumDataSort(sumProc, topN, new MinTimeComparator());
			break;
		case MAX:
			dataList = getSumDataSort(sumProc, topN, new MaxTimeComparator());
			break;
		case AVG:
			dataList = getSumDataSort(sumProc, topN, new AvgTimeComparator());
			break;
		case DURATION:
			dataList = getSumDataSort(sumProc, topN, new DurationComparator());
			break;
		case COUNT:
			dataList = getSumDataSort(sumProc, topN, new CountComparator());
			break;
		case THREASHOLD2:
			dataList = getSumDataSort(sumProc, topN, new Threashold2Comparator());
			break;
		default:
			dataList = getSumDataSort(sumProc, topN, new DurationComparator());
			break;

		}

		for (SummaryData data : dataList) {
			dataSource.add(data.getStringIndex(), data.getKey(),
					data.getType(), 
					repUtil.getMilliToSec(data.getMinVal()),
					repUtil.getMilliToSec(data.getMaxVal()),
					repUtil.getMilliToSec(data.getAvgVal()),
					repUtil.getMilliToSec(data.getTotalDuration()),
					data.getTotalCount(),
					data.getApprovedCount(),
					data.getThreashold1Count(),
					data.getThreashold2Count());
		}
		return dataSource;
	}
	
	
	
	/**
	 * Gets the sum data sorted max time.
	 *
	 * @param sumProc the sum proc
	 * @param topN            the top n
	 * @param compare the compare
	 * @return the sum data sorted max time
	 */
	public List<SummaryData> getSumDataSort(SummaryProcessor sumProc, int topN,
			Comparator<SummaryData> compare) {

		@SuppressWarnings("unchecked")
		Collection<DataSourceI> dataSource = (Collection<DataSourceI>) sumProc
				.getDataSource();

		List<DataSourceI> dataSourceList = new ArrayList<DataSourceI>(
				dataSource);
		List<SummaryData> sortedList = new ArrayList<SummaryData>();

		if (topN > dataSourceList.size()) {
			topN = dataSourceList.size();
		}

		// convert totalList and set the index
		for (int i = 0; i < dataSourceList.size(); i++) {
			SummaryData sumData = (SummaryData) dataSourceList.get(i);
			sumData.setIndex(i + 1);
			sortedList.add(sumData);

		}

		Collections.sort(sortedList, compare);

		// decending
		Collections.reverse(sortedList);
		return sortedList.subList(0, topN);

	}
	

	
	/**
	 * Gets the sum data sorted max time.
	 *
	 * @param sumProc the sum proc
	 * @param topN            the top n
	 * @param compare the compare
	 * @return the sum data sorted max time
	 */
	public List<SummaryData> getSumDataSortFilteredByType(SummaryProcessor sumProc, int topN,
			Comparator<SummaryData> compare, TypeEnum type) {

		@SuppressWarnings("unchecked")
		Collection<DataSourceI> dataSource = (Collection<DataSourceI>) sumProc
				.getDataSource();

		List<DataSourceI> dataSourceList = new ArrayList<DataSourceI>(
				dataSource);
		List<SummaryData> sortedList = new ArrayList<SummaryData>();

		if (topN > dataSourceList.size()) {
			topN = dataSourceList.size();
		}

		// convert totalList and set the index
		for (int i = 0; i < dataSourceList.size(); i++) {
			SummaryData sumData = (SummaryData) dataSourceList.get(i);
			// filter only to add the type
			if (type.getTypeName().equals(sumData.getType())) {
				sumData.setIndex(i + 1);
				sortedList.add(sumData);
			}

		}

		Collections.sort(sortedList, compare);

		// decending
		Collections.reverse(sortedList);
		return sortedList.subList(0, topN);
	}
	/**
	 * Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Date> getDateColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.DATE_CM,
				type.dateType());
	}
	
	
	/**
	 * Gets the hh str column.
	 *
	 * @param columnTitle the column title
	 * @return the hh str column
	 */
	public TextColumnBuilder<String> getHhStrColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.HH_STR_CM,
				type.stringType());
	}
	
	/**
	 * Gets the hh str column.
	 *
	 * @param columnTitle the column title
	 * @return the hh str column
	 */
	public TextColumnBuilder<String> getDayStrColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.DAY_STR_CM,
				type.stringType());
	}
	
	/**
	 * Gets the index column.
	 *
	 * @param columnTitle the column title
	 * @return the index column
	 */
	public TextColumnBuilder<String> getIndexColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.INDEX_CM,
				type.stringType());
	}

	/**
	 * Gets the type column.
	 *
	 * @param columnTitle the column title
	 * @return the type column
	 */
	public TextColumnBuilder<String> getTypeColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.TYPE_CM,
				type.stringType());
	}

	/**
	 * Gets the element column.
	 *
	 * @param columnTitle the column title
	 * @return the element column
	 */
	public TextColumnBuilder<String> getElementColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.ELEMENT_CM,
				type.stringType());
	}

	
	/**
	 * Gets the min val column.
	 *
	 * @param columnTitle the column title
	 * @return the min val column
	 */
	public TextColumnBuilder<Double> getMinValColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.MIN_VAL_CM,
				type.doubleType()).setPattern("##,##0.00");
	}

	/**
	 * Gets the max val column.
	 *
	 * @param columnTitle the column title
	 * @return the max val column
	 */
	public TextColumnBuilder<Double> getMaxValColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.MAX_VAL_CM,
				type.doubleType()).setPattern("##,##0.00");
	}
	
	/**
	 * Gets the avg val column.
	 *
	 * @param columnTitle the column title
	 * @return the avg val column
	 */
	public TextColumnBuilder<Double> getAvgValColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.AVG_VAL_CM,
				type.doubleType()).setPattern("##,##0.00");
	}
	
	/**
	 * Gets the dur val column.
	 *
	 * @param columnTitle the column title
	 * @return the dur val column
	 */
	public TextColumnBuilder<Double> getDurValColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.DUR_VAL_CM,
				type.doubleType()).setPattern("##,##0.00");
	}

	/**
	 * Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getCountColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.COUNT_VAL_CM,
				type.integerType());
	}
	
	/*
	 * Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getThreashold1CountColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.THRESHOLD_COUNT_1_CM,
				type.integerType());
	}
	 
	/*
	* Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getthreashold2CountColumn(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.THRESHOLD_COUNT_2_CM,
				type.integerType());
	}
	
	/*
	* Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getApprovedCount(String columnTitle) {
		return col.column(columnTitle,
				SumDataReportDS.APPROVED_COUNT_CM,
				type.integerType());
	}
}
