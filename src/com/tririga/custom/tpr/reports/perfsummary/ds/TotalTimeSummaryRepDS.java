package com.tririga.custom.tpr.reports.perfsummary.ds;

import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;

import com.tririga.custom.tpr.processor.totalsum.DateComparator;
import com.tririga.custom.tpr.processor.totalsum.TotalSummaryData;
import com.tririga.custom.tpr.processor.totalsum.TotalSummaryProcessor;
import com.tririga.custom.tpr.reports.DataSourceI;
import com.tririga.custom.tpr.reports.perfsummary.ReportUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class TotalSummaryRepDS.
 */
public class TotalTimeSummaryRepDS {
	
	/** The Constant ELEMENT_CM. */
	public static final String DATE_CM = "Date";

	/** The Constant ELEMENT_CM. */
	public static final String ELEMENT_CM = "element";
	
	/** The Constant COUNT_CM. */
	public static final String EXTF_COUNT_CM = "extf_count";
	
	/** The Constant COUNT_CM. */
	public static final String EXTFQ_COUNT_CM = "extfq_count";
	/** The Constant COUNT_CM. */
	public static final String REPORTS_COUNT_CM = "reports_count";
	/** The Constant COUNT_CM. */
	public static final String SO_COUNT_CM = "so_count";
	/** The Constant COUNT_CM. */
	public static final String WFASYNC_COUNT_CM = "wfasync_count";

	/** The Constant COUNT_CM. */
	public static final String WFSYNC_COUNT_CM = "wfsync_count";
	/** The Constant COUNT_CM. */
	public static final String EXTF_DUR_CM = "extf_dur";
	
	/** The Constant COUNT_CM. */
	public static final String EXTFQ_DUR_CM = "extfq_dur";
	/** The Constant COUNT_CM. */
	public static final String REPORTS_DUR_CM = "reports_cdur";
	/** The Constant COUNT_CM. */
	public static final String SO_DUR_CM = "so_dur";
	/** The Constant COUNT_CM. */
	public static final String WFASYNC_DUR_CM = "wfasync_dur";

	/** The Constant COUNT_CM. */
	public static final String WFSYNC_DUR_CM = "wfsync_dur";
		
	
	/**
	 * Creates the total summary ds.
	 *
	 * @param totalSumProc the total sum proc
	 * @return the JR data source
	 */
	public JRDataSource createTimeTotalSummaryDS(TotalSummaryProcessor totalSumProc) {

		DRDataSource dataSource = new DRDataSource(TotalTimeSummaryRepDS.DATE_CM,
				TotalTimeSummaryRepDS.EXTF_COUNT_CM,
				TotalTimeSummaryRepDS.EXTFQ_COUNT_CM,
				TotalTimeSummaryRepDS.REPORTS_COUNT_CM,
				TotalTimeSummaryRepDS.SO_COUNT_CM,
				TotalTimeSummaryRepDS.WFASYNC_COUNT_CM,
				TotalTimeSummaryRepDS.WFSYNC_COUNT_CM,
				TotalTimeSummaryRepDS.EXTF_DUR_CM,
				TotalTimeSummaryRepDS.EXTFQ_DUR_CM,
				TotalTimeSummaryRepDS.REPORTS_DUR_CM,
				TotalTimeSummaryRepDS.SO_DUR_CM,
				TotalTimeSummaryRepDS.WFASYNC_DUR_CM,
				TotalTimeSummaryRepDS.WFSYNC_DUR_CM);
		DateFormat dfDay = new SimpleDateFormat("yyyy-MM-dd");
		for(TotalSummaryData data: getTimeTotalSummaryData(totalSumProc)) {
			ReportUtil repUtil = new ReportUtil();

			dataSource.add(	dfDay.format(data.getFirstRecord().getStartTime()),
					data.getExtFormulaCalcTotal(),
					data.getExtFormulaQueueTotal(),
					data.getReportsTotal(),
					data.getsOTransitionTotal(),
					data.getWorkflowAsyncTotal(),
					data.getWorkflowSyncTotal(),
					repUtil.getMilliToMin(data.getExtFormulaCalcDurTotal()),
					repUtil.getMilliToMin(data.getExtFormulaQueueTotal()),
					repUtil.getMilliToMin(data.getReportsDurTotal()),
					repUtil.getMilliToMin(data.getsOTransitionDurTotal()),
					repUtil.getMilliToMin(data.getWorkflowAsyncDurTotal()),
					repUtil.getMilliToMin(data.getWorkflowSyncDurTotal()));
			
		}

		return dataSource;
	}

	/**
	 * Gets the total summary data.
	 *
	 * @param totalSumProc the total sum proc
	 * @return the total summary data
	 */
	public TotalSummaryData getTotalSummaryData(
			TotalSummaryProcessor totalSumProc) {
		TotalSummaryData data = null;
		@SuppressWarnings("unchecked")
		Collection<DataSourceI> totalSource = (Collection<DataSourceI>) totalSumProc
				.getDataSource();
		if (totalSource.size() == 1) { 
			data = (TotalSummaryData) totalSource.iterator().next();
		}

		return data;
	}
	
	/**
	 * Gets the total summary data.
	 *
	 * @param totalSumProc the total sum proc
	 * @return the total summary data
	 */
	public List<TotalSummaryData> getTimeTotalSummaryData( TotalSummaryProcessor totalSumProc) {

		List<TotalSummaryData> rtnVal = new ArrayList<TotalSummaryData>();
		@SuppressWarnings("unchecked")
		Collection<DataSourceI> totalSource = (Collection<DataSourceI>) totalSumProc
				.getDataSource();
		for(DataSourceI tmp : totalSource) {
			rtnVal.add((TotalSummaryData) tmp);
		}
		Collections.sort(rtnVal, new DateComparator());
		return rtnVal;
	}
	
	/**
	 * Gets the item column.
	 *
	 * @param columnTitle the column title
	 * @return the item column
	 */
	public TextColumnBuilder<String> getItemColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalTimeSummaryRepDS.ELEMENT_CM,
				type.stringType());
	}
	
	/**
	 * Gets the item column.
	 *
	 * @param columnTitle the column title
	 * @return the item column
	 */
	public TextColumnBuilder<String> getStartDateColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalTimeSummaryRepDS.DATE_CM,
				type.stringType());
	}
	
	/**
	 * Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getExtfCountColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalTimeSummaryRepDS.EXTF_COUNT_CM,
				type.integerType());
	}
	

	/**
	 * Gets the dur column.
	 *
	 * @param columnTitle the column title
	 * @return the dur column
	 */
	public TextColumnBuilder<Double> getExtfDurColumn(String columnTitle) {
		return col.column(columnTitle,
			TotalTimeSummaryRepDS.EXTF_DUR_CM, 
			type.doubleType()).setPattern("##,##0.00");
	}
	
	
	/**
	 * Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getExtfqCountColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalTimeSummaryRepDS.EXTFQ_COUNT_CM,
				type.integerType());
	}
	

	/**
	 * Gets the dur column.
	 *
	 * @param columnTitle the column title
	 * @return the dur column
	 */
	public TextColumnBuilder<Double> getExtfqDurColumn(String columnTitle) {
		return col.column(columnTitle,
			TotalTimeSummaryRepDS.EXTFQ_DUR_CM, 
			type.doubleType()).setPattern("##,##0.00");
	}
	
	/**
	 * Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getReportsCountColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalTimeSummaryRepDS.REPORTS_COUNT_CM,
				type.integerType());
	}
	

	/**
	 * Gets the dur column.
	 *
	 * @param columnTitle the column title
	 * @return the dur column
	 */
	public TextColumnBuilder<Double> getReportsDurColumn(String columnTitle) {
		return col.column(columnTitle,
			TotalTimeSummaryRepDS.REPORTS_DUR_CM, 
			type.doubleType()).setPattern("##,##0.00");
	}
	
	/**
	 * Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getSoCountColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalTimeSummaryRepDS.SO_COUNT_CM,
				type.integerType());
	}
	

	/**
	 * Gets the dur column.
	 *
	 * @param columnTitle the column title
	 * @return the dur column
	 */
	public TextColumnBuilder<Double> getSoDurColumn(String columnTitle) {
		return col.column(columnTitle,
			TotalTimeSummaryRepDS.SO_DUR_CM, 
			type.doubleType()).setPattern("##,##0.00");
	}
	
	/**
	 * Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getWfAsyncCountColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalTimeSummaryRepDS.WFASYNC_COUNT_CM,
				type.integerType());
	}
	

	/**
	 * Gets the dur column.
	 *
	 * @param columnTitle the column title
	 * @return the dur column
	 */
	public TextColumnBuilder<Double> geWfAsyncDurColumn(String columnTitle) {
		return col.column(columnTitle,
			TotalTimeSummaryRepDS.WFASYNC_DUR_CM, 
			type.doubleType()).setPattern("##,##0.00");
	}

	/**
	 * Gets the count column.
	 *
	 * @param columnTitle the column title
	 * @return the count column
	 */
	public TextColumnBuilder<Integer> getWfSyncCountColumn(String columnTitle) {
		return col.column(columnTitle,
				TotalTimeSummaryRepDS.WFSYNC_COUNT_CM,
				type.integerType());
	}
	

	/**
	 * Gets the dur column.
	 *
	 * @param columnTitle the column title
	 * @return the dur column
	 */
	public TextColumnBuilder<Double> geWfSyncDurColumn(String columnTitle) {
		return col.column(columnTitle,
			TotalTimeSummaryRepDS.WFSYNC_DUR_CM, 
			type.doubleType()).setPattern("##,##0.00");
	}
}
