package com.tririga.custom.tpr.reports.perfsummary;

// TODO: Auto-generated Javadoc
/**
 * The Enum SumDataSortOrder.
 */
public enum SumDataSortOrder {

	/** The min. */
	MIN,
	/** The max. */
	MAX,
	/** The avg. */
	AVG,
	/** The duration. */
	DURATION,
	/** The count. */
	COUNT,
	/** The element. */
	ELEMENT,
	THREASHOLD2
}
