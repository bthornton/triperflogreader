/*
 * 
 */
package com.tririga.custom.tpr.reports.perfsummary;

import static net.sf.dynamicreports.report.builder.DynamicReports.cht;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.report;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;

import java.util.Collections;
import java.util.List;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.jasper.builder.export.JasperPdfExporterBuilder;
import net.sf.dynamicreports.report.base.expression.AbstractSimpleExpression;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.chart.AreaChartBuilder;
import net.sf.dynamicreports.report.builder.component.Components;
import net.sf.dynamicreports.report.builder.group.ColumnGroupBuilder;
import net.sf.dynamicreports.report.builder.style.FontBuilder;
import net.sf.dynamicreports.report.definition.ReportParameters;
import net.sf.dynamicreports.report.exception.DRException;

import com.tririga.custom.tpr.data.TypeEnum;
import com.tririga.custom.tpr.processor.summary.DurationComparator;
import com.tririga.custom.tpr.processor.summary.SummaryData;
import com.tririga.custom.tpr.processor.summary.SummaryProcessor;
import com.tririga.custom.tpr.processor.timesum.TimeRangeEnum;
import com.tririga.custom.tpr.processor.timesum.TimeSummaryProcessor;
import com.tririga.custom.tpr.processor.totalsum.TimeTotalSummaryProcessor;
import com.tririga.custom.tpr.processor.totalsum.TotalSummaryData;
import com.tririga.custom.tpr.processor.totalsum.TotalSummaryProcessor;
import com.tririga.custom.tpr.reports.Report;
import com.tririga.custom.tpr.reports.graphical.Templates;
import com.tririga.custom.tpr.reports.perfsummary.ds.SumDataReportDS;
import com.tririga.custom.tpr.reports.perfsummary.ds.TimeSumDataDS;
import com.tririga.custom.tpr.reports.perfsummary.ds.TotalSummaryRepDS;
import com.tririga.custom.tpr.reports.perfsummary.ds.TotalTimeSummaryRepDS;
import com.tririga.custom.tpr.reports.system.MinMaxFileStore;
import com.tririga.custom.tpr.reports.system.SummaryFileStore;

// TODO: Auto-generated Javadoc
/**
 * The Class SummaryReport.
 */
public class SummaryReport implements Report {

	private int threashold1 = 30000; //30 sec
	private int threashold2 = 300000; // 5 min
	
	List<TypeEnum> excludeElementTypes;
	List<String> excludeElements;
	
	/** The sum proc. */
	private SummaryProcessor sumProc;

	/** The total sum proc. */
	private TotalSummaryProcessor totalSumProc;

	/** The time sum proc. */
	private TimeSummaryProcessor timeSumProc;
	
	/** The time sum proc. */
	private TimeSummaryProcessor timeDaySumProc;
	
	private TimeTotalSummaryProcessor timeTotalSumProc;

	public SummaryReport(int threashold1, int threashold2, List<TypeEnum> excludeElementTypes, List<String> excludeElements ) {
		this.threashold1 = threashold1;
		this.threashold2 = threashold2;
		this.excludeElementTypes = excludeElementTypes;
		this.excludeElements = excludeElements;
		
	}
	// second pass
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tririga.custom.tpr.reports.Report#configObservers()
	 */
	@Override
	public void configFirstObservers() {
		// Set up parms for Processors and Stores
		sumProc = new SummaryProcessor(true, this.threashold1, this.threashold2, excludeElementTypes,excludeElements );
		totalSumProc = new TotalSummaryProcessor(true, excludeElementTypes);
		timeTotalSumProc = new TimeTotalSummaryProcessor(true, TimeRangeEnum.DAY,this.excludeElementTypes);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tririga.custom.tpr.reports.Report#configSecondObservers()
	 */
	@Override
	public void configSecondObservers() {
		// unregist the first Observers
		// need a better way to do this
		sumProc.removeObserver();
		totalSumProc.removeObserver();
		timeTotalSumProc.removeObserver();
		SumDataReportDS sumDataRepDS = new SumDataReportDS();

		timeSumProc = new TimeSummaryProcessor(TimeRangeEnum.HOUR,
				sumDataRepDS.getSumDataSort(sumProc, 10,
						new DurationComparator()), this.threashold1, this.threashold2, this.excludeElementTypes, excludeElements);

		timeDaySumProc = new TimeSummaryProcessor(TimeRangeEnum.DAY,
				sumDataRepDS.getSumDataSort(sumProc, 10,
						new DurationComparator()), this.threashold1, this.threashold2, this.excludeElementTypes, excludeElements);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tririga.custom.tpr.reports.Report#runReport()
	 */
	@Override
	public void runReport() throws Exception {

		runSystemReport();

		runGraphicalReport();

	}

	/**
	 * Run system report.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private void runSystemReport() throws Exception {
		long currentTime = System.currentTimeMillis();

		SumDataReportDS sumDataRepDS = new SumDataReportDS();
		List<SummaryData> sortedList = sumDataRepDS.getSumDataSort(sumProc,
				100, new DurationComparator());

		// decending
		Collections.reverse(sortedList);

		// Write to File
		SummaryFileStore fileStore = new SummaryFileStore();
		fileStore.setOutputFileName("Summary_" + currentTime + ".csv");

		fileStore.store(sortedList);

		// Write Min Max Values to File
		MinMaxFileStore minMaxFileStore = new MinMaxFileStore();
		minMaxFileStore.setOutputFileName("MinMax_" + currentTime + ".csv");
		minMaxFileStore.store(sortedList);

	}

	/**
	 * Run graphical report.
	 */
	private void runGraphicalReport() {
		TotalSummaryRepDS totalSumRepDS = new TotalSummaryRepDS();

		TotalSummaryData totalSumData = totalSumRepDS.getTotalSummaryData(totalSumProc);

		FontBuilder boldFont = stl.fontArialBold().setFontSize(12);
		
		

		JasperPdfExporterBuilder pdfExporter = DynamicReports.export.pdfExporter("output/PerfSumReport-"+System.currentTimeMillis()+".pdf");
				
		
		try {

			report().setTemplate(Templates.reportTemplate)
					.columns(totalSumRepDS.getItemColumn("Element"),
							totalSumRepDS.getDurShortestColumn("Min Dur (Min)"),
							totalSumRepDS.getDurLongestColumn("Max Dur (Min)"),
							totalSumRepDS.getDurColumn("Total Duration (Min)"),
							totalSumRepDS.getCountColumn("Count"))
							
					.title(Templates
							.createTitleComponent("Performance Summary Report"))
					.summary(
							cht.bar3DChart()
									.setTitle("Total Count Summary")
									.setSubtitle(
											"" + totalSumData.getStartDate() + " - "
													+ totalSumData.getEndDate())
									.setTitleFont(boldFont)
									.setCategory(
											totalSumRepDS
													.getItemColumn("Element"))
									.setShowLegend(false)
									.setShowValues(true)
									.series(cht.serie(totalSumRepDS
											.getCountColumn("Count")))
									.setCategoryAxisFormat(
											cht.axisFormat().setLabel(
													"Elements")),
							cht.bar3DChart()
									.setTitle("Total Duration Summary (min)")
									.setSubtitle(
											"" + totalSumData.getStartDate() + " - "
													+ totalSumData.getEndDate())
									.setTitleFont(boldFont)
									.setCategory(
											totalSumRepDS
													.getItemColumn("Element"))
									.setShowLegend(false)
									.setShowValues(true)
									.series(cht.serie(totalSumRepDS
											.getDurColumn("Total Duration")))
									.setCategoryAxisFormat(
											cht.axisFormat().setLabel(
													"Elements")),
							Components.pageBreak(),
							cmp.subreport(this.getTotalDurationSummaryByDay(
															"Total Duration Summary (min) - By Day",
															SumDataSortOrder.DURATION, 10)),
							Components.pageBreak(),
							cmp.subreport(this.getTopNSubreport(
									"Top 10 - Total Duration (sec)",
									SumDataSortOrder.DURATION, 10)),
							//Components.pageBreak(),
							cmp.subreport(this.getTopNSubreport(
									"Top 10 - Max Time (sec)",
									SumDataSortOrder.MAX, 10)),
							Components.pageBreak(),
							cmp.subreport(this.getTopNSubreport(
									"Top 30 - Elements > 5 min",
									SumDataSortOrder.THREASHOLD2, 30)),
							Components.pageBreak(),
								cmp.subreport(this.getDayTopNSubreport(
										"Top 10 - Total Duration Elements by Day",totalSumData,
										SumDataSortOrder.DURATION, 100)),
							Components.pageBreak(),
							cmp.subreport(this.getTimeTopNSubreport(
									"Top 10 - Total Duration Elements by Hour", totalSumData,
									SumDataSortOrder.DURATION, 100)))
					.pageFooter(Templates.footerComponent)
					.setDataSource(
							totalSumRepDS.createTotalSummaryDS(totalSumProc))
					.show()
					.toPdf(pdfExporter);
		} catch (DRException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Gets the subreport.
	 * 
	 * @param title
	 *            the title
	 * @param sortOrder
	 *            the sort order
	 * @param topN
	 *            the top n
	 * @return the subreport
	 */
	private JasperReportBuilder getTotalDurationSummaryByDay(String title,
			SumDataSortOrder sortOrder, int topN) {
		TotalTimeSummaryRepDS totalTimeSumRepDS = new TotalTimeSummaryRepDS();

		//		SumDataReportDS sumDataRepDS = new SumDataReportDS();

		JasperReportBuilder report = report();

		report.setTemplate(Templates.reportTemplate)
				.title(cmp.text(title).setStyle(Templates.bold12CenteredStyle))
				.columns(totalTimeSumRepDS.getExtfDurColumn("Ext Formula"),
						totalTimeSumRepDS.getExtfqDurColumn("Ext Formula Q"),
						totalTimeSumRepDS.getReportsDurColumn("Reports"),
						totalTimeSumRepDS.getSoDurColumn("Smart Object"),
						totalTimeSumRepDS.geWfSyncDurColumn("Sync Workflows"),
						totalTimeSumRepDS.geWfAsyncDurColumn("Async Workflows"))
				.summary(
						cht.stackedBar3DChart()
						.setTitle("Total Duration Summary (min)")
						.setCategory(
								totalTimeSumRepDS
										.getStartDateColumn("Day"))
						.setShowLegend(true)
						.setShowValues(true)
						.series(cht.serie(totalTimeSumRepDS.getExtfDurColumn("Ext Formula")),
								cht.serie(totalTimeSumRepDS.getExtfqDurColumn("Ext Formula Q")),
								cht.serie(totalTimeSumRepDS.getReportsDurColumn("Reports")),
								cht.serie(totalTimeSumRepDS.getSoDurColumn("Smart Object")),
								cht.serie(totalTimeSumRepDS.geWfSyncDurColumn("Sync Workflows")),
								cht.serie(totalTimeSumRepDS.geWfAsyncDurColumn("Async Workflows"))
								)
						.setCategoryAxisFormat(
								cht.axisFormat().setLabel(
										"Days")))
				.setDataSource(
						totalTimeSumRepDS.createTimeTotalSummaryDS(timeTotalSumProc));
		return report;
	}
	/**
	 * Gets the subreport.
	 * 
	 * @param title
	 *            the title
	 * @param sortOrder
	 *            the sort order
	 * @param topN
	 *            the top n
	 * @return the subreport
	 */
	private JasperReportBuilder getTopNSubreport(String title,
			SumDataSortOrder sortOrder, int topN) {

		SumDataReportDS sumDataRepDS = new SumDataReportDS();

		JasperReportBuilder report = report();

		report.setTemplate(Templates.reportTemplate)
				.title(cmp.text(title).setStyle(Templates.bold12CenteredStyle))
				.columns(sumDataRepDS.getIndexColumn("Index").setWidth(5),
						sumDataRepDS.getElementColumn("Element").setWidth(40),
						sumDataRepDS.getTypeColumn("Type").setWidth(12),
						sumDataRepDS.getMinValColumn("Min").setWidth(7),
						sumDataRepDS.getMaxValColumn("Max").setWidth(7),
						sumDataRepDS.getAvgValColumn("Avg").setWidth(7),
						sumDataRepDS.getDurValColumn("Dur").setWidth(7),
						sumDataRepDS.getCountColumn("Total Count").setWidth(7),
						sumDataRepDS.getApprovedCount("Acc Count").setWidth(7),
						sumDataRepDS.getThreashold1CountColumn("> 30 Sec < 5 min").setWidth(7),
						sumDataRepDS.getthreashold2CountColumn("> 5 min").setWidth(7))
				.summary(
						cht.layeredBarChart()
								// .setTitle(title)
								// .setTitleFont(boldFont)
								.seriesBarWidths(1.0, 0.5, 0.25)
								.setCategory(sumDataRepDS.getIndexColumn("Index"))
								.setHeight(175)
								.series(cht.serie(sumDataRepDS.getMaxValColumn("Max")),
										cht.serie(sumDataRepDS.getAvgValColumn("Avg")),
										cht.serie(sumDataRepDS.getMinValColumn("Min")))
								.setCategoryAxisFormat(
										cht.axisFormat().setLabel("Item")))
				.setDataSource(
						sumDataRepDS.createSumDataDS(sumProc, sortOrder, topN));
		return report;
	}

	/**
	 * Gets the subreport.
	 * 
	 * @param title
	 *            the title
	 * @param sortOrder
	 *            the sort order
	 * @param topN
	 *            the top n
	 * @return the subreport
	 */
	private JasperReportBuilder getTimeTopNSubreport(String title, TotalSummaryData totalSumdata,
			SumDataSortOrder sortOrder, int topN) {
		TimeSumDataDS timeSumDataDS = new TimeSumDataDS();

		JasperReportBuilder report = report();

		AreaChartBuilder areaChart = cht
				.areaChart()
//				.setTitle("Area chart")
				.setCategory(timeSumDataDS.getHhStrColumn("Hour"))
				.series(cht.serie(timeSumDataDS.getMaxValColumn("Max")),
						cht.serie(timeSumDataDS.getAvgValColumn("Avg")),
						cht.serie(timeSumDataDS.getMinValColumn("Min")))
				.setCategoryAxisFormat(cht.axisFormat().setLabel("Time"));

		ColumnGroupBuilder elementGroup = DynamicReports.grp
				.group(timeSumDataDS.getElementColumn("Element")).footer(areaChart).keepTogether().showColumnHeaderAndFooter();
				//.setHeaderPrintWhenExpression(new PrintWhenExpression());

		report.setTemplate(Templates.reportTemplate)
				.title(cmp.text(title).setStyle(Templates.bold12CenteredStyle))
				.setShowColumnTitle(false) 
				.columns(timeSumDataDS.getHhStrColumn("Hour").setWidth(5),
						timeSumDataDS.getTypeColumn("Type").setWidth(30),
						timeSumDataDS.getMinValColumn("Min (Sec.)").setWidth(10),
						timeSumDataDS.getMaxValColumn("Max (Sec.)").setWidth(10),
						timeSumDataDS.getAvgValColumn("Avg (Sec.)").setWidth(10),
						timeSumDataDS.getDurValColumn("Dur (Sec.)").setWidth(10),
						timeSumDataDS.getCountColumn("Total Count").setWidth(10),
						timeSumDataDS.getApprovedCount("Acc Count").setWidth(7),
						timeSumDataDS.getThreashold1CountColumn("> 30 Sec < 5 min").setWidth(7),
						timeSumDataDS.getthreashold2CountColumn("> 5 min").setWidth(7))

						.setDetailPrintWhenExpression(new PrintWhenExpression())

				.groupBy(elementGroup)
				.setDataSource(
						timeSumDataDS.createTimeSumDataDS(timeSumProc, totalSumdata,
								SumDataSortOrder.ELEMENT, topN));
			
		return report;
	}
	/**
	 * Gets the subreport.
	 * 
	 * @param title
	 *            the title
	 * @param sortOrder
	 *            the sort order
	 * @param topN
	 *            the top n
	 * @return the subreport
	 */
	private JasperReportBuilder getDayTopNSubreport(String title,TotalSummaryData totalSumdata,
			SumDataSortOrder sortOrder, int topN) {
		TimeSumDataDS timeSumDataDS = new TimeSumDataDS();

		JasperReportBuilder report = report();

		AreaChartBuilder areaChart = cht
				.areaChart()
//				.setTitle("Area chart")
				.setCategory(timeSumDataDS.getDayStrColumn("Day"))
				.series(cht.serie(timeSumDataDS.getMaxValColumn("Max")),
						cht.serie(timeSumDataDS.getAvgValColumn("Avg")),
						cht.serie(timeSumDataDS.getMinValColumn("Min")))
				.setCategoryAxisFormat(cht.axisFormat().setLabel("Time"));

		ColumnGroupBuilder elementGroup = DynamicReports.grp
				.group(timeSumDataDS.getElementColumn("Element")).footer(areaChart).keepTogether().showColumnHeaderAndFooter().headerWithSubtotal();
				//.setHeaderPrintWhenExpression(new PrintWhenExpression());

		report.setTemplate(Templates.reportTemplate)
				.title(cmp.text(title).setStyle(Templates.bold12CenteredStyle))
				.setShowColumnTitle(false) 
				.columns(timeSumDataDS.getDayStrColumn("Day").setWidth(10),
						timeSumDataDS.getTypeColumn("Type").setWidth(30),
						timeSumDataDS.getMinValColumn("Min (Sec.)").setWidth(10),
						timeSumDataDS.getMaxValColumn("Max (Sec.)").setWidth(10),
						timeSumDataDS.getAvgValColumn("Avg (Sec.)").setWidth(10),
						timeSumDataDS.getDurValColumn("Dur (Sec.)").setWidth(10),
						timeSumDataDS.getCountColumn("Total Count").setWidth(10),
						timeSumDataDS.getApprovedCount("Acc Count").setWidth(7),
						timeSumDataDS.getThreashold1CountColumn("> 30 Sec < 5 min").setWidth(7),
						timeSumDataDS.getthreashold2CountColumn("> 5 min").setWidth(7))
						.setDetailPrintWhenExpression(new PrintWhenExpression())

				.groupBy(elementGroup)
				.setDataSource(
						timeSumDataDS.createTimeSumDataDS(timeDaySumProc,totalSumdata,
								SumDataSortOrder.ELEMENT, topN));
			
		return report;
	}
	
	private class PrintWhenExpression extends AbstractSimpleExpression<Boolean> {     
		/**
		 * 
		 */
		private static final long serialVersionUID = -6311961912938586934L;

		public Boolean evaluate(ReportParameters reportParameters) {        
			Boolean rtnVal = Boolean.TRUE;
		
			System.out.println("Name:"+reportParameters.getValue(TimeSumDataDS.COUNT_VAL_CM).getClass().getName());
		
			Integer count2 = reportParameters.getValue(TimeSumDataDS.COUNT_VAL_CM);
			
			if(count2.intValue() == 0) {
				rtnVal = Boolean.FALSE;
			}
			return rtnVal;
		}   
	}
}

