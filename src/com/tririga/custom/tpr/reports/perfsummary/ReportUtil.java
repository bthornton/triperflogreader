package com.tririga.custom.tpr.reports.perfsummary;

// TODO: Auto-generated Javadoc
/**
 * The Class ReportUtil.
 */
public class ReportUtil {

	/**
	 * Gets the milli to min.
	 * 
	 * @param milli
	 *            the milli
	 * @return the milli to min
	 */
	public double getMilliToMin(long milli) {
		return getMilliToSec(milli) / 60.0;
	}

	/**
	 * Gets the milli to sec.
	 * 
	 * @param milli
	 *            the milli
	 * @return the milli to sec
	 */
	public double getMilliToSec(long milli) {
		return milli / 1000.0;
	}
}
