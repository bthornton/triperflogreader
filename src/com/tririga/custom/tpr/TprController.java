/*
 * 
 */
package com.tririga.custom.tpr;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import com.tririga.custom.tpr.data.TypeEnum;
import com.tririga.custom.tpr.reader.TprFileReader;
import com.tririga.custom.tpr.reports.Report;
import com.tririga.custom.tpr.reports.perfsummary.SummaryReport;

// TODO: Auto-generated Javadoc
/**
 * The Class TprController.
 */
public class TprController {

	public static final int THREASHOLD1 = 30000; //30 sec
	public static final int THREASHOLD2 = 300000; // 5 min
	/** The dir name. */
	private String dirName = "";
	/** The dir name. */
	private String type = "";
	
	private String configDir = "";

	/** The file names. */
	private List<String> fileNames = new ArrayList<String>();

	private List<Report> reports = new ArrayList<Report>();
	
	private List<TypeEnum> excludeElementTypes = new ArrayList<TypeEnum>();

	/** The options. */
	private Options options = new Options();

	/** The cmd. */
	private CommandLine cmd = null;

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		TprController tprController = null;
		try {
			tprController = new TprController(args);

			// tprController.addObserver(new SystemOutProcessor());

			tprController.configFirstRun();
			tprController.run();

			
			tprController.configSecondRun();
			tprController.run();

			tprController.runReports();

			System.out.println("Complete");

		} catch (Exception e) {
			e.printStackTrace();
			if (tprController != null) {
				tprController.printHelp();
			}
		}

	}

	/**
	 * Instantiates a new tpr controller.
	 * 
	 * @param args
	 *            the args
	 * @throws Exception
	 *             the exception
	 */
	public TprController(String[] args) throws Exception {
		// move this to main
		cmd = createOptions(args);

		setDirName(cmd);
		setType(cmd);
		setConfigPath(cmd);
		getPropValues();

		if (cmd.hasOption("h")) {
			printHelp();
		}

		
		ArrayList<String> excludeElements = new ArrayList<String>();
//		excludeElements.add("[BO: FinancialTransaction, Current State: BoStateImpl[Name=new]]");
		
		
		reports.add(new SummaryReport(TprController.THREASHOLD1, TprController.THREASHOLD2, excludeElementTypes, excludeElements));
//		reports.add(new TrendReport(TprController.THREASHOLD1, TprController.THREASHOLD2, excludeElementTypes, excludeElements));


	}

	public void configFirstRun() throws Exception {
		for (Report report : reports) {
			report.configFirstObservers();
		}

	}

	public void configSecondRun() throws Exception {
		for (Report report : reports) {
			report.configSecondObservers();
		}
	}

	/**
	 * Run.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void run() throws Exception {
		TprFileReader tprFileReader = new TprFileReader();

		for (String fileName : fileNames) {
			System.out.println(fileName);
			tprFileReader.setFileName(dirName + "/" + fileName);
			tprFileReader.process();
		}

	}

	public void runReports() throws Exception {
		for (Report report : reports) {
			report.runReport();
		}

	}

	/**
	 * Sets the dir name.
	 * 
	 * @param cmd
	 *            the new dir name
	 */
	private void setDirName(CommandLine cmd) {
		if (cmd.hasOption("dir")) {
			this.dirName = cmd.getOptionValue("dir");
			System.out.println("dirName[" + this.dirName + "]");

			getFileNames();
		}
	}

	/**
	 * Sets the dir name.
	 * 
	 * @param cmd
	 *            the new dir name
	 */
	private void setType(CommandLine cmd) {
		if (cmd.hasOption("type")) {
			this.type = cmd.getOptionValue("type");
			System.out.println("type[" + this.type + "]");

		}
	}
	
	/**
	 * Sets the dir name.
	 * 
	 * @param cmd
	 *            the new dir name
	 */
	private void setConfigPath(CommandLine cmd) {
		if (cmd.hasOption("configDir")) {
			this.configDir = cmd.getOptionValue("configDir");
			System.out.println("configDir[" + this.configDir + "]");

		}
	}

	/**
	 * Gets the file names.
	 * 
	 * @return the file names
	 */
	private void getFileNames() {
		File[] files = new File(dirName).listFiles();

		for (File file : files) {
			if (file.isFile()) {
				fileNames.add(file.getName());
			}
		}

	}

	/**
	 * Prints the help.
	 */
	public void printHelp() {
		HelpFormatter f = new HelpFormatter();
		f.printHelp("OptionsTip", options);
	}

	/**
	 * Creates the options.
	 * 
	 * @param args
	 *            the args
	 * @return the command line
	 * @throws Exception
	 *             the exception
	 */
	public CommandLine createOptions(String[] args) throws Exception {
		CommandLine cmd = null;
		options = new Options();

		OptionBuilder.withArgName("dir");
		OptionBuilder.hasArg();
		OptionBuilder
				.withDescription("dir name that hold muliple performance log files");
		options.addOption(OptionBuilder.create("dir"));

		OptionBuilder.withArgName("configDir");
		OptionBuilder.hasArg();
		OptionBuilder
				.withDescription("dir name where the config.properties is located");
		options.addOption(OptionBuilder.create("configDir"));

		
		
		OptionBuilder.withArgName("h");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("Print the help options");
		options.addOption(OptionBuilder.create("h"));

		OptionBuilder.withArgName("type");
		OptionBuilder.hasArg();
		OptionBuilder
				.withDescription("sum or time... time breaks it down by day");
		options.addOption(OptionBuilder.create("type"));

		CommandLineParser parser = new BasicParser();
		try {
			cmd = parser.parse(options, args);
		} catch (Exception e) {
			printHelp();
			throw e;
		}
		


		return cmd;
	}
	
	public String getPropValues() throws IOException {
		 
		String result = "";
		Properties prop = new Properties();
		String propFileName = "config.properties";
 
//		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configDir+"/"+propFileName);
		InputStream inputStream = new FileInputStream(configDir+"/config.properties");
 
		prop.load(inputStream);
 

		// get the property value and print it out
		Boolean incWFA = new Boolean(prop.getProperty("INCLUDE_WORKFLOW_ASYNC"));
		Boolean incEFC = new Boolean(prop.getProperty("INCLUDE_EF_CALC"));
		Boolean incRep = new Boolean(prop.getProperty("INCLUDE_REPORT"));
		Boolean incSOTrans = new Boolean(prop.getProperty("INCLUDE_SO_TRANS"));
		Boolean incWFS = new Boolean(prop.getProperty("INCLUDE_WORKFLOW_SYNC"));
 
		if(!incWFA) {
			excludeElementTypes.add(TypeEnum.WORKFLOW_ASYNC);
		}
		if(!incEFC) {
			excludeElementTypes.add(TypeEnum.EF_CALC);
		}
		if(!incRep) {
			excludeElementTypes.add(TypeEnum.REPORT);
		}
		if(!incSOTrans) {
			excludeElementTypes.add(TypeEnum.SO_TRANS);
		}
		if(!incWFS) {
			excludeElementTypes.add(TypeEnum.WORKFLOW_SYNC);
		}
		
		System.out.println("Exclude the following types:"+excludeElementTypes);
		return result;
	}

}
