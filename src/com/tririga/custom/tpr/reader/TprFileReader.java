package com.tririga.custom.tpr.reader;

import java.io.FileReader;
import java.io.IOException;

import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

import com.tririga.custom.tpr.data.TprBaseData;
import com.tririga.custom.tpr.processor.Processor;
import com.tririga.custom.tpr.reader.mapper.TprMapper;

public class TprFileReader {

	private String fileName = "";

	private Processor proc = Processor.getInstance();

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void process() throws Exception {

		
		ICsvListReader listReader = null;
		try {
			listReader = new CsvListReader(new FileReader(fileName),
					CsvPreference.TAB_PREFERENCE);

			listReader.getHeader(false); // header can't be used with
											// CsvListReader

			while ((listReader.read()) != null) {

				// use different mappers depending on the number of columns
				// System.out.println("length:"+listReader.length());
				// System.out.println(listReader);
				TprMapper mapper = TprMapperFactory.getTprMapper(listReader
						.length());

				final CellProcessor[] processors = mapper.getCellProcessor();

				TprBaseData tprData = mapper.map(listReader
						.executeProcessors(processors));

				// System.out.println(tprData);
				proc.notifyObservers(tprData);

				// print status to standard out
				int lineNum = listReader.getLineNumber();
				// System.out.println("lineNum:"+lineNum);
				if ((lineNum % 1000) == 0) {
					if ((lineNum % 100000) == 0) {
						System.out.println(".");
					} else {
						System.out.print(".");
					}
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (listReader != null) {
				listReader.close();
			}
		}

	}

	public void store() throws Exception {
		// proc.store();
	}
}
