package com.tririga.custom.tpr.reader;

import com.tririga.custom.tpr.reader.mapper.TprBaseMapper;
import com.tririga.custom.tpr.reader.mapper.TprBaseMapper16;
import com.tririga.custom.tpr.reader.mapper.TprBaseMapper20;
import com.tririga.custom.tpr.reader.mapper.TprExtFormulaMapper;
import com.tririga.custom.tpr.reader.mapper.TprMapper;
import com.tririga.custom.tpr.reader.mapper.TprReportMapper;
import com.tririga.custom.tpr.reader.mapper.TprWorkflowMapper;

public class TprMapperFactory {

	public static TprMapper getTprMapper(int columnSize) {
		TprMapper rtnMapper = null;
		// System.out.println("columnSize="+columnSize);
		switch (columnSize) {
		case TprBaseMapper16.COLUMN_SIZE:
			rtnMapper = new TprBaseMapper16();
			break;
		case TprBaseMapper.COLUMN_SIZE:
			rtnMapper = new TprBaseMapper();
			break;
		case TprExtFormulaMapper.COLUMN_SIZE:
			rtnMapper = new TprExtFormulaMapper();
			break;
		case TprReportMapper.COLUMN_SIZE:
			rtnMapper = new TprReportMapper();
			break;
		case TprWorkflowMapper.COLUMN_SIZE:
			rtnMapper = new TprWorkflowMapper();
			break;
		case TprBaseMapper20.COLUMN_SIZE:
			rtnMapper = new TprBaseMapper20();
			break;
		default:
			// / All mappers have this in common
			rtnMapper = new TprBaseMapper();
		}

		return rtnMapper;
	}
}
