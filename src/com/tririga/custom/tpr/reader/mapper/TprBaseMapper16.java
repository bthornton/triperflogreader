package com.tririga.custom.tpr.reader.mapper;

import java.util.List;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;

import com.tririga.custom.tpr.data.TprBaseData;

// TODO: Auto-generated Javadoc
/**
 * The Class TprBaseMapper.
 */
public class TprBaseMapper16 implements TprMapper {

	/** The Constant COLUMN_SIZE. */
	public static final int COLUMN_SIZE = 16;

	/** The all processors. */
	private final CellProcessor[] processors = new CellProcessor[] {
			new NotNull(), // log4J header
			new NotNull(), // ignore number
			new NotNull(), // Type header
			new NotNull(), // Type
			new NotNull(), // Key header
			new NotNull(), // Key
			new NotNull(), // Duration header
			new Optional(new ParseInt()), // Duration
			new NotNull(), // StartTime header
			new Optional(new ParseLong()), // StartTime
			new NotNull(), // EndTime header
			new Optional(new ParseLong()), // EndTime
			new NotNull(), // UserId header
			new Optional(new ParseLong()), // UserId
			new NotNull(), // RecordId header
			new Optional(new ParseLong()), // RecordId
			new NotNull(), // Source header
			new NotNull() }; // Source

	/**
	 * Gets the cell processor.
	 * 
	 * @return the cell processor
	 */
	public CellProcessor[] getCellProcessor() {
		return processors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tririga.custom.tpr.reader.mapper.TprMapper#map(java.util.List)
	 */
	public TprBaseData map(List<Object> tprList) {
		TprBaseData rtnData = new TprBaseData();
		TprMapperUtil util = new TprMapperUtil();

		util.map(rtnData, tprList);

		return rtnData;
	}

}
