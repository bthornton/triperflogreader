package com.tririga.custom.tpr.reader.mapper;

import java.util.List;

import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;

import com.tririga.custom.tpr.data.TprBaseData;
import com.tririga.custom.tpr.data.TprDataExtFormula;

// TODO: Auto-generated Javadoc
/**
 * The Class TprBaseMapper.
 */
public class TprExtFormulaMapper implements TprMapper {

	/** The Constant COLUMN_SIZE. */
	public static final int COLUMN_SIZE = 24;

	/** The all processors. */
	private final CellProcessor[] processors = new CellProcessor[] {
			new NotNull(), // log4J header
			new NotNull(), // ignore number
			new NotNull(), // Type header
			new NotNull(), // Type
			new NotNull(), // Key header
			new NotNull(), // Key
			new NotNull(), // Duration header
			new ParseInt(), // Duration
			new NotNull(), // StartTime header
			new ParseLong(), // StartTime
			new NotNull(), // EndTime header
			new ParseLong(), // EndTime
			new NotNull(), // UserId header
			new ParseLong(), // UserId
			new NotNull(), // RecordId header
			new ParseLong(), // RecordId
			new NotNull(), // Source header
			new NotNull(), // Source
			new NotNull(), // Target Field Info header
			new NotNull(), // Target Field Info Steps
			new NotNull(), // Source Row Id header
			new ParseLong(), // Source Row Id
			new NotNull(), // Expression header
			new NotNull() }; // Expression Template

	/**
	 * Gets the cell processor.
	 * 
	 * @return the cell processor
	 */
	public CellProcessor[] getCellProcessor() {
		return processors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tririga.custom.tpr.reader.mapper.TprMapper#map(java.util.List)
	 */
	public TprBaseData map(List<Object> tprList) {
		TprDataExtFormula rtnData = new TprDataExtFormula();
		TprMapperUtil util = new TprMapperUtil();

		// Map Base fields
		util.map(rtnData, tprList);

		// Map extended fields
		rtnData.setTargetFieldInfo((String) tprList.get(19));
		rtnData.setSourceRowId((long) tprList.get(21));
		rtnData.setExpression((String) tprList.get(23));

		return rtnData;
	}

}
