package com.tririga.custom.tpr.reader.mapper;

import java.util.List;

import com.tririga.custom.tpr.data.TprData;

public class TprMapperUtil {

	/**
	 * new NotNull(), // log4J header new NotNull(), // ignore number new
	 * NotNull(), // Type header new NotNull(), // Type new NotNull(), // Key
	 * header new NotNull(), // Key new NotNull(), // Duration header new
	 * ParseLong(), // Duration new NotNull(), // StartTime header new
	 * ParseLong(), // StartTime new NotNull(), // EndTime header new
	 * ParseLong(), // EndTime new NotNull(), // UserId header new ParseLong(),
	 * // UserId new NotNull(), // RecordId header new ParseLong(), // RecordId
	 * new NotNull(), // Source header new NotNull()}; // Source
	 * 
	 * 
	 * @param tprBaseData
	 *            the tpr base data
	 * @param tprList
	 *            the tpr list
	 */
	public void map(TprData tprBaseData, List<Object> tprList) {
		tprBaseData.setType((String) tprList.get(3));
		tprBaseData.setKey((String) tprList.get(5));
		tprBaseData.setDuration((int) tprList.get(7));
		tprBaseData.setStartTime((long) tprList.get(9));
		tprBaseData.setEndTime((long) tprList.get(11));
		if (tprList.get(13) != null) {
			tprBaseData.setUserId((long) tprList.get(13));
		}
		if (tprList.get(15) != null) {
			tprBaseData.setRecordId((long) tprList.get(15));
		}
		tprBaseData.setSource((String) tprList.get(17));

	}
}
