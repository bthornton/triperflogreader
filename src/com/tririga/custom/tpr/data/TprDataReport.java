/*
 * 
 */
package com.tririga.custom.tpr.data;

// TODO: Auto-generated Javadoc
/**
 * The Class TprDataReport.
 */
public class TprDataReport extends TprBaseData {

	/** The response info. */
	private String responseInfo = "";

	/** The request info. */
	private String requestInfo = "";

	/**
	 * Gets the response info.
	 * 
	 * @return the response info
	 */
	public String getResponseInfo() {
		return responseInfo;
	}

	/**
	 * Sets the response info.
	 * 
	 * @param responseInfo
	 *            the new response info
	 */
	public void setResponseInfo(String responseInfo) {
		this.responseInfo = responseInfo;
	}

	/**
	 * Gets the request info.
	 * 
	 * @return the request info
	 */
	public String getRequestInfo() {
		return requestInfo;
	}

	/**
	 * Sets the request info.
	 * 
	 * @param requestInfo
	 *            the new request info
	 */
	public void setRequestInfo(String requestInfo) {
		this.requestInfo = requestInfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tririga.custom.tpr.data.TprData#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + " TprDataReport [responseInfo="
				+ responseInfo + ", requestInfo=" + requestInfo + "]";
	}

}
