package com.tririga.custom.tpr.data;

// TODO: Auto-generated Javadoc
/**
 * The Class TprDataWorkflow.
 */
public class TprDataWorkflow extends TprBaseData {

	/** The num steps. */
	private long numSteps = 0;

	/** The wf templet id. */
	private double wfTempletId = 0;

	/** The wf id. */
	private long wfId = 0;

	/** The info. */
	private String info = "";

	/**
	 * Gets the num steps.
	 * 
	 * @return the num steps
	 */
	public long getNumSteps() {
		return numSteps;
	}

	/**
	 * Sets the num steps.
	 * 
	 * @param numSteps
	 *            the new num steps
	 */
	public void setNumSteps(long numSteps) {
		this.numSteps = numSteps;
	}

	/**
	 * Gets the wf id.
	 * 
	 * @return the wf id
	 */
	public long getWfId() {
		return wfId;
	}

	/**
	 * Sets the wf id.
	 * 
	 * @param wfId
	 *            the new wf id
	 */
	public void setWfId(long wfId) {
		this.wfId = wfId;
	}

	/**
	 * Gets the wf templet id.
	 * 
	 * @return the wf templet id
	 */
	public double getWfTempletId() {
		return wfTempletId;
	}

	/**
	 * Sets the wf templet id.
	 * 
	 * @param wfTempletId
	 *            the new wf templet id
	 */
	public void setWfTempletId(double wfTempletId) {
		this.wfTempletId = wfTempletId;
	}

	/**
	 * Gets the info.
	 * 
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * Sets the info.
	 * 
	 * @param info
	 *            the new info
	 */
	public void setInfo(String info) {
		this.info = info;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tririga.custom.tpr.data.TprData#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + " TprDataWorkflow [numSteps=" + numSteps
				+ ", wfTempletId=" + wfTempletId + ", wfId=" + wfId + ", info="
				+ info + "]";
	}

}
