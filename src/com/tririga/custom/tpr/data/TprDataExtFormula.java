package com.tririga.custom.tpr.data;

// TODO: Auto-generated Javadoc
/**
 * The Class TprDataExtFormula.
 */
public class TprDataExtFormula extends TprBaseData {

	/** The target field info. */
	private String targetFieldInfo = "";

	/** The source row id. */
	private long sourceRowId = 0;

	/** The expression. */
	private String expression = "";

	/**
	 * Gets the target field info.
	 * 
	 * @return the target field info
	 */
	public String getTargetFieldInfo() {
		return targetFieldInfo;
	}

	/**
	 * Sets the target field info.
	 * 
	 * @param targetFieldInfo
	 *            the new target field info
	 */
	public void setTargetFieldInfo(String targetFieldInfo) {
		this.targetFieldInfo = targetFieldInfo;
	}

	/**
	 * Gets the source row id.
	 * 
	 * @return the source row id
	 */
	public long getSourceRowId() {
		return sourceRowId;
	}

	/**
	 * Sets the source row id.
	 * 
	 * @param sourceRowId
	 *            the new source row id
	 */
	public void setSourceRowId(long sourceRowId) {
		this.sourceRowId = sourceRowId;
	}

	/**
	 * Gets the expression.
	 * 
	 * @return the expression
	 */
	public String getExpression() {
		return expression;
	}

	/**
	 * Sets the expression.
	 * 
	 * @param expression
	 *            the new expression
	 */
	public void setExpression(String expression) {
		this.expression = expression;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tririga.custom.tpr.data.TprData#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + " TprDataExtFormula [targetFieldInfo="
				+ targetFieldInfo + ", sourceRowId=" + sourceRowId
				+ ", expression=" + expression + "]";
	}

}
