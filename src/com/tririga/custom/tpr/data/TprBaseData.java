package com.tririga.custom.tpr.data;

// TODO: Auto-generated Javadoc
/**
 * The Class TprData.
 */
public class TprBaseData implements TprData {

	/** The type. */
	private String type = "";

	/** The key. */
	private String key = "";

	/** The duration. */
	private int duration = 0;

	/** The start time. */
	private long startTime = 0;

	/** The end time. */
	private long endTime = 0;

	/** The user id. */
	private long userId = 0;

	/** The record id. */
	private long recordId = 0;

	/** The source. */
	private String source = "";

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the key.
	 * 
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 * 
	 * @param key
	 *            the new key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Gets the duration.
	 * 
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * Sets the duration.
	 * 
	 * @param duration
	 *            the new duration
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * Gets the start time.
	 * 
	 * @return the start time
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * Sets the start time.
	 * 
	 * @param startTime
	 *            the new start time
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	/**
	 * Gets the end time.
	 * 
	 * @return the end time
	 */
	public long getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time.
	 * 
	 * @param endTime
	 *            the new end time
	 */
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * Gets the record id.
	 * 
	 * @return the record id
	 */
	public long getRecordId() {
		return recordId;
	}

	/**
	 * Sets the record id.
	 * 
	 * @param recordId
	 *            the new record id
	 */
	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	/**
	 * Gets the source.
	 * 
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Sets the source.
	 * 
	 * @param source
	 *            the new source
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TprData [type=" + type + ", key=" + key + ", duration="
				+ duration + ", startTime=" + startTime + ", endTime="
				+ endTime + ", userId=" + userId + ", recordId=" + recordId
				+ ", source=" + source + "]";
	}

}
