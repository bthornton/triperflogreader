package com.tririga.custom.tpr.data;

public enum TypeEnum {

	WORKFLOW_SYNC("Workflow - Sync"), 
	EF_CALC("Extended Formula - Calculation"), 
	SO_TRANS("Smart Object Transition"), 
	REPORT("Report"), 
	WORKFLOW_ASYNC("Workflow - Async"),
	
	BAD_VALUE("Bad Value")
			; // semicolon needed when fields / methods
									// follow

	private final String typeName;

	TypeEnum(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		return this.typeName;
	}

	public static TypeEnum getTypeEnum(String type) {
		  for(TypeEnum typeE : TypeEnum.values()){
			   if(type.equalsIgnoreCase(typeE.getTypeName())){
			    return typeE;
			   }
			  }
		  return BAD_VALUE;
			 
	}
}
