type	key	minVal	maxVal	totalDuration	totalCount
Workflow - Async	cstDMHelper - DataLoad - Associate Asset Invoices with Asset Leases	7	9	16	2
Workflow - Async	cstDMHelper - DataLoad - Associate PLI's to Asset Lease	19	120	139	2
Extended Formula - Calculation	[BO: cstDataMigrationHelper, FieldName: triDurationDU]	0	1	2	4
Workflow - Async	cstDMHelper - DataLoad - Generate Payment Schedule Associated to Asset leases	13	18	31	2
Workflow - Async	cstDMHelper - DataLoad - Remove Duplicate Vendor Records	9448	202827	212275	2
Workflow - Async	cstDMHelper - DataLoad - Remove Single Quote from Lessor Site Name	6	7	13	2
Report	cstExternalCompany  - DATALOAD - New Vendor ROOT from PEARL	1198	1325	2523	2
Workflow - Async	cstDMHelper - DataLoad - Update Asset Lease ID for Generate Payment Schedule	11	15	26	2
Report	cstExternalCompany - DATALOAD - List All Duplicate External Companies	3	73	45324	8631
Workflow - Async	cstDCVendorJob - System DC Process Job - Import Vendor Data	8316301	8316301	8316301	1
Smart Object Transition	[BO: triDataConnectJobLog, Current State: BoStateImpl[Name=null]]	14	426	25612	721
Workflow - Async	cstDMHelper - DataLoad - Approve Generated Payment Schedule	14	61	75	2
Workflow - Async	cstDMHelper - DataLoad - Generate Payment Schedule	7	9	16	2
Extended Formula - Calculation	[BO: cstDataMigrationHelper, FieldName: triInput2NU]	0	0	0	5
Workflow - Async	cstDMHelper - DataLoad - Map Lessor Site Name with Asset Leases	7	10	17	2
Workflow - Async	cstDMHelper - DataLoad - Associate Stop Payment to Payment Schedule	13	17	30	2
Workflow - Async	cstDMHelper - Dataload - Map ID to PLI Id for Payment Line Items	6	7	13	2
Workflow - Async	cstDMHelper - DataLoad - Remove Single Quote from External Companies	6	9	15	2
Smart Object Transition	[BO: Organization, Current State: BoStateImpl[Name=triActive]]	108	4124	5187	10
Smart Object Transition	[BO: Organization, Current State: BoStateImpl[Name=null]]	7391	11914	5528461	656
Report	cstExternalCompany - DATALOAD - List All Filtered Vendors	1454	1757	3211	2
Workflow - Async	cstDMHelper - DataLoad - Update Site Details in FA Numbers	6	7	13	2
