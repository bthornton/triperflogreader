package com.tririga.custom.tpr.reader;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TprFileReaderTest {

	public static final String TEST_FILE_NAME = "testdata/performance_server10_20131010.txt";
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		TprFileReader tprFileReader = new TprFileReader();
		
		tprFileReader.setFileName(TprFileReaderTest.TEST_FILE_NAME);
		
		try {
			tprFileReader.process();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
